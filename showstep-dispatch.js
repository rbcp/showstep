
var class_showstepEffectClass = Ds.GetClassID("showstepEffectClass");


include("./src/utils.js");

function tree_find (ob, children_fn, fn, skip_root, call_on_children_first) {
    function walk1 (ob, skip) {
        var children = children_fn(ob);
        if (! call_on_children_first && ! skip) {
            var r = fn(ob);
            if (r) {
                return r;
            }
        }
        for (var i = 0, n = children.length; i < n; ++i) {
            var child = children[i];
            r = walk1(child, false);
            if (r) {
                return r;
            }
        }
        if (call_on_children_first && ! skip) {
            r = fn(ob);
            if (r) {
                return r;
            }
        }
        return null;
    }
    return walk1(ob, skip_root || false);
}


/*
 * Main
 */

function insert (thing) {
    if (object_exists(thing) &&
        Ds.GetObjectClassID(thing) == class_showstepEffectClass)
    {
        var path = Ds.GetObjectAttr(thing, "path");
    } else { // it's a string pathname
        print("deprecated insert command: "+thing);
        var script_play_path = Ds.GetObjectAttr("script", "play");
        path = resolve_script(thing, script_play_path, script_play_path);
    }
    var have_shim = tree_find(
        "showstep5", Ds.GetObjectChildNames,
        function (effect_object) {
            var sp = Ds.GetObjectAttr(effect_object, "provide").split(" ");
            return (sp.indexOf("EffectShowstep3Shim") > -1);
        }, true, false);
    if (have_shim) {
        // pass command to active shim
        Ds.SetObjectAttr("showstep5", "command",
                         "message('EffectShowstep3Shim','showstep3_insert',"+
                         JSON.stringify(path)+")");
        Ds.ExecuteObjectCommand("s5", "on");
    } else {
        // create new showstep3shim
        var shim_cursor = gensym(thing, "showstep5EffectClass");
        Ds.SetObjectAttr(shim_cursor, "path", "$Effects/EffectShowstep3Shim.js");
        Ds.SetObjectAttr(shim_cursor, "assetPath", drop_filename(path));
        Ds.AllocObjectArray(shim_cursor, "option", 1);
        Ds.SetObjectArrayElem(shim_cursor, "option", 0, "path="+path);
        Ds.SetObjectAttr("showstep5", "command", "insert('"+shim_cursor+"')");
        Ds.ExecuteObjectCommand("s5", "on");
    }
}

function play (thing) {
    if (object_exists(thing) &&
        Ds.GetObjectClassID(thing) == class_showstepEffectClass)
    {
        var path = Ds.GetObjectAttr(thing, "path");
    } else { // it's a string pathname
        print("deprecated play command: "+thing);
        var script_play_path = Ds.GetObjectAttr("script", "play");
        path = resolve_script(thing, script_play_path, script_play_path);
    }
    var shim_cursor = gensym(thing, "showstep5EffectClass");
    Ds.SetObjectAttr(shim_cursor, "path", "$Effects/EffectShowstep3Shim.js");
    Ds.SetObjectAttr(shim_cursor, "assetPath", drop_filename(path));
    Ds.AllocObjectArray(shim_cursor, "option", 1);
    Ds.SetObjectArrayElem(shim_cursor, "option", 0, "path="+path);
    Ds.SetObjectAttr("showstep5", "command", "start('"+shim_cursor+"')");
    Ds.ExecuteObjectCommand("s5", "on");
}
