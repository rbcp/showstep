
include(join_paths(fx_load_path, "scripted-toggle.js"));

function FxScriptedRadio (show, name, object, on_script, options) {
    if (! ("group" in options)) {
        options.group = "default";
    }
    FxScriptedToggle.call(this, show, name, object, on_script, options);
}
FxScriptedRadio.prototype = {
    constructor: FxScriptedRadio,
    __proto__: FxScriptedToggle.prototype,
    toString: function () {
        return "#<FxScriptedRadio "+this.object+" "+this.on_script+">";
    }
};

effect_register('scripted-radio', FxScriptedRadio);
