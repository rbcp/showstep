
/*

This fx module calls a command.

Options:

 - once: limit to one play when true

*/

function FxCommand (show, name, command, options) {
    ButtonFx.call(this, show, name, options);
    if (! command) {
        throw "command: command required";
    }
    this.command = command;
    if ("once" in options) {
        this.once = options.once;
    }
}
FxCommand.prototype = {
    constructor: FxCommand,
    __proto__: ButtonFx.prototype,
    command: null,
    once: false,
    _played: false,
    toString: function () {
        return "#<FxCommand "+this.command+">";
    },

    // Button interface
    //
    call: function () {
        print("command: "+this.command);
        if (! this.once || ! this._played) {
            ButtonFx.prototype.call.call(this, true);
            this._played = true;
            Ds.SendStringCommand(this.command);
        } else {
            log("This button may only be played once.");
        }
    },
    cleanup: function () {
        ButtonFx.prototype.cleanup.call(this);
    }
};


effect_register('command', FxCommand);
