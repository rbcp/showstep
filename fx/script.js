
/*

This fx module calls a script.  The script may be given as an absolute
path beginning with "$", a relative path beginning with "." to be resolved
against the current value of script.play, or a relative path not beginning
with "$" or "." to be resolved against the current value of
script.directory.

If a -cleanup script exists for the given script, that script will be
called during section cleanup.  The cleanup filename is determined as
follows:

 - If the script ends in '-play.ds', replace '-play' with '-cleanup'.
 - Otherwise, append '-cleanup' to the script's basename.

Options:

 - once: limit to one play when true

*/

function FxScript (show, name, script, options) {
    ButtonFx.call(this, show, name, options);
    if (! script) {
        throw "script: script required";
    }
    script = resolve_script(script, null, show.scriptroot);
    var script_filename = take_filename(script);
    var base_path = drop_filename(script);
    var base_filename = script_filename.replace(/(-play)?\.ds$/, "");
    var cleanup_script = join_paths(base_path, base_filename + "-cleanup.ds");
    if (file_readable(cleanup_script)) {
        this.cleanup_script = cleanup_script;
    }
    this.script = script;
    if ("once" in options) {
        this.once = options.once;
    }
}
FxScript.prototype = {
    constructor: FxScript,
    __proto__: ButtonFx.prototype,
    script: null,
    cleanup_script: null,
    once: false,
    _played: false,
    toString: function () {
        return "#<FxScript "+this.script+">";
    },

    // Button interface
    //
    call: function () {
        print("script: "+this.script);
        if (! this.once || ! this._played) {
            ButtonFx.prototype.call.call(this, true);
            this._played = true;
            Ds.SetObjectAttrUsingRef(dsrefs.script.play, this.script);
        } else {
            log("This button may only be played once.");
        }
    },
    cleanup: function () {
        ButtonFx.prototype.cleanup.call(this);
        if (this.cleanup_script) {
            Ds.SetObjectAttrUsingRef(dsrefs.script.play, this.cleanup_script);
        }
    }
};


effect_register('script', FxScript);
