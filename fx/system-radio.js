
include(join_paths(fx_load_path, "system-toggle.js"));

function FxSystemRadio (show, name, object, options) {
    if (! ("group" in options)) {
        options.group = "default";
    }
    FxSystemToggle.call(this, show, name, object, options);
}
FxSystemRadio.prototype = {
    constructor: FxSystemRadio,
    __proto__: FxSystemToggle.prototype,
    toString: function () {
        return "#<FxSystemRadio "+this.object+">";
    }
};

effect_register('system-radio', FxSystemRadio);
