
/*

The scripted-toggle fx manages a toggle button for an object that it
turned on by a script.  An -off script and/or a -define script may also
optionally be provided.  In the fx call when defining the button, you
supply the name of the -on script, and this module will automatically
check for the existence of the other scripts by substituding '-on' for
'-off' and '-define' in the filename.

The -on script may be given as an absolute path, or, if it does not begin
with '$' or '.' it will be resolved against script.directory.

The 'status' attribute of the object is used to determine whether the
object is on or off, so the -on script should set status on, and the -off
script (if present) should set status off or delete the object.

This module can also automatically call a -define script at button setup
time, if the option define=auto is given.  The purpose of this is to
preload media.  In this case, the -define script should define the the
object and set its status as 'off'.

The behavior of this fx with its different possible uses are as follows.

 - "-on" only:

   When toggling the object off, the object intensity fades to 0 over 3
   seconds, then the object is deleted. (XXX: to-do: and its children?)

 - "-on" and "-off" only:

   When toggling the object off, the -off script is called.  The -off
   script should set status off or delete the object.

 - "-on" and "-define" only (with define=auto):

   When toggling the object off, the object intensity fades to 0 over 3
   seconds, then the object's status is set to off.

 - "-on", "-off", and "-define" (with define=auto):

   Same as for "-on" and "-off" only.  The -off script should set the
   object's status to off, and not delete it.

*/

function FxScriptedToggle (show, name, object, on_script, options) {
    ButtonFx.call(this, show, name, options);
    if (! object) {
        throw "scripted-toggle: object name required";
    }
    if (! on_script) {
        throw "scripted-toggle: on_script required";
    }
    on_script = resolve_script(on_script, null, show.scriptroot);
    var on_script_filename = take_filename(on_script);
    var base_path = drop_filename(on_script);
    var base_filename = on_script_filename.replace(/-on\.ds$/, "");
    this.object = object;
    this.on_script = on_script;
    var off_script = join_paths(base_path, base_filename + "-off.ds");
    if (file_readable(off_script)) {
        this.off_script = off_script;
    }
    if ("define" in options) {
        if (options.define == "auto") {
            this.define = true;
        } else {
            Ds.SendMessageWarning("scripted-toggle define=false is no longer needed. ("+this.object+")");
        }
    }
    var define_script = join_paths(base_path, base_filename + "-define.ds");
    if (this.define && file_readable(define_script)) {
        this.define_script = define_script;
    }
    if ("enable" in options) {
        this.enable = options.enable;
    }
    if ("notes" in options) {
        this.notes = options.notes;
    }
    if (this.enable) {
        // the enable option causes the button to be treated as a require,
        // i.e., it will get turned on via the require_on method.
        this.require_p = true;
    }
}
FxScriptedToggle.prototype = {
    constructor: FxScriptedToggle,
    __proto__: ButtonFx.prototype,
    object: null,
    on_script: null,
    off_script: null,
    define_script: null,
    define: null,
    enable: false,
    notes: false,
    toString: function () {
        return "#<FxScriptedToggle "+this.object+" "+this.on_script+">";
    },

    // Radio interface
    //
    radio_get_status: function () {
        try {
            return Ds.GetObjectAttr(this.object, "status");
        } catch (e) {
            return 0;
        }
    },
    radio_self_off: function () {
        if (this.off_script) {
            Ds.SetObjectAttrUsingRef(dsrefs.script.play, this.off_script);
        } else if (Ds.GetObjectID(this.object) > -1) {
            //XXX this is weird behavior
            if (this.define_script) {
                Ds.SendScriptCommands(this.name+"-off",
                                      "\t"+this.object+" intensity 0 duration 3\n"+
                                      "+3\t"+this.object+" status off");
            } else {
                Ds.SendScriptCommands(this.name+"-off",
                                      "\t"+this.object+" intensity 0 duration 3\n"+
                                      "+3\t"+this.object+" delete");
            }
        }
    },

    // Button interface
    //
    setup: function (button, label, button_refs, props) {
        ButtonFx.prototype.setup.call(this, button, label, button_refs, props);
        props.cornerradius = 25;
        if (this.define_script) {
            Ds.SetObjectAttrUsingRef(dsrefs.script.play, this.define_script);
        }
        if (this.radio_get_status()) {
            props.bordercolor = "30 30 100";
        } else if (this.enable) {
            props.bordercolor = "30 30 100";
            Ds.SetObjectAttrUsingRef(dsrefs.script.play, this.on_script);
            if (this.notes) {
                cp.format_notes({ show: this.show, tag: this.notes }, "current");
            }
        }
    },
    call: function () {
        var status = this.radio_get_status();
        ButtonFx.prototype.call.call(this, ! status);
        if (status) {
            Ds.SetObjectAttrUsingRef(this.button_refs.bordercolor,
                                     { r: 65, g: 65, b: 65 });
            this.radio_self_off();
            if (this.notes) {
                cp.format_notes(this.show.current_section(true), "current");
            }
        } else {
            Ds.SetObjectAttrUsingRef(dsrefs.script.play, this.on_script);
            Ds.SetObjectAttrUsingRef(this.button_refs.bordercolor,
                                     { r: 30, g: 30, b: 100 });
            if (this.notes) {
                cp.format_notes({ show: this.show, tag: this.notes }, "current");
            }
        }
    },
    cleanup: function () {
        ButtonFx.prototype.cleanup.call(this);
        this.radio_self_off();
    },

    // Require interface
    //
    require_on: function () {
        Ds.SetObjectAttrUsingRef(dsrefs.script.play, this.on_script);
        if (this.notes) {
            cp.format_notes({ show: this.show, tag: this.notes }, "current");
        }
    }
};


effect_register('scripted-toggle', FxScriptedToggle);
