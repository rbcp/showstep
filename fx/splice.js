

function FxSplice (show, name, playlist_name, options) {
    ButtonFx.call(this, show, name, options);
    if (! playlist_name) {
        throw "splice: playlist name required";
    }
    this.playlist_name = playlist_name;
    if ("replace" in options) {
        this.replace = parseInt(options.replace);
    }
}
FxSplice.prototype = {
    constructor: FxSplice,
    __proto__: ButtonFx.prototype,
    playlist_name: null,
    replace: 0,
    toString: function () {
        return "#<FxSplice "+this.playlist_name+">";
    },

    // Button interface
    //
    setup: function (button, label, button_refs, props) {
        ButtonFx.prototype.setup.call(this, button, label, button_refs, props);
        props.borderwidth = 4;
    },
    call: function () {
        ButtonFx.prototype.call.call(this, true);
        showstep_splice(this.playlist_name, this.replace);
    },
    cleanup: function () {
        ButtonFx.prototype.cleanup.call(this);
    }
};


effect_register('splice', FxSplice);
