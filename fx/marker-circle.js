

function FxMarkerCircle (show, name, target, options) {
    ButtonFx.call(this, show, name, options);
    if (! target) {
        throw "marker-circle: target required";
    }
    this.target = target;
}
FxMarkerCircle.prototype = {
    constructor: FxMarkerCircle,
    __proto__: ButtonFx.prototype,
    target: null,
    toString: function () {
        return "#<FxMarkerCircle "+this.target+">";
    },

    scale_distance: function (pos) {
        var d = Math.sqrt(pos.x * pos.x + pos.y * pos.y + pos.z * pos.z);
        var desired = 2 * 9.4607304725808e15;
        var scale = desired / d;
        return { x: pos.x * scale, y: pos.y * scale, z: pos.z * scale};
    },
    get_scale: function (x) {
        x = x * 9.4607304725808e15;
        return { x:x, y:x, z:x };
    },

    // Button interface
    //
    setup: function (button, label, button_refs, props) {
        ButtonFx.prototype.setup.call(this, button, label, button_refs, props);
    },
    call: function () {
        ButtonFx.prototype.call.call(this, true);
        var ly = 9.4607304725808e15;
        var dsob = gensym("fx_marker", "lineModelClass");
        Ds.SetObjectAttr(dsob, "model", "$Content/Library/Models/Markers/circle_100m.x");
        Ds.SetObjectAttr(dsob, "scale", this.get_scale(0.0001));
        Ds.SetObjectAttr(dsob, "color", { r: 100, g: 70, b: 100 });
        Ds.SetObjectAttr(dsob, "intensity", 0);
        Ds.ObjectFacePosition(dsob, { x: 0, y: 0, z: 0}, "-z");
        Ds.AddObjectChild("scenenormal", dsob);
        var target = this.target;
        if (typeof target == "string") {
            Ds.SendStringCommand(target+" on");
            Ds.Wait(0.1);
            var pos = Ds.GetObjectAttr(target, "position");
            target = this.scale_distance(pos);
        }
        Ds.SetObjectAttr(dsob, "position", target);
        var get_scale = this.get_scale;
        sleep(0.5, function () {
            Ds.SetObjectAttr(dsob, "intensity", 100);
            Ds.SetObjectAttr(dsob, "scale", get_scale(0.0035), { total: 2 });
        });
        sleep(1.5, function () { Ds.SetObjectAttr(dsob, "intensity", 0, { total: 1 }); });
        sleep(3.5, function () { Ds.DeleteObject(dsob); });
    }
};


effect_register('marker-circle', FxMarkerCircle);
