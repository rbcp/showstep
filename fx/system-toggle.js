

function FxSystemToggle (show, name, object, options) {
    ButtonFx.call(this, show, name, options);
    if (! object) {
        throw "system-toggle: object name required";
    }
    this.object = object;
    if ("enable" in options) {
        //XXX being read as a string so any value is truthy?
        this.enable = options.enable;
    }
    if ("notes" in options) {
        this.notes = options.notes;
    }
}
FxSystemToggle.prototype = {
    constructor: FxSystemToggle,
    __proto__: ButtonFx.prototype,
    object: null,
    isgroup: null,
    statusobject: null,
    enable: false,
    notes: false,
    toString: function () {
        return "#<FxSystemToggle "+this.object+">";
    },

    // Radio interface
    //
    radio_get_status: function () {
        try {
            if (this.isgroup === false) {
                return Ds.GetObjectAttr(this.object, "status");
            } else if (this.isgroup == null) {
                var groupclass = Ds.GetClassID("systemObjectGroupClass");
                var objectclass = Ds.GetObjectClassID(this.object);
                if (objectclass == groupclass) {
                    this.isgroup = true;
                    var statusobject = Ds.GetObjectArrayElem(this.object, "member", 0);
                    this.statusobject = statusobject.name;
                } else {
                    this.isgroup = false;
                }
                return this.radio_get_status();
            } else {
                return Ds.GetObjectAttr(this.statusobject, "status");
            }
        } catch (e) {
            return 0;
        }
    },
    radio_self_off: function () {
        Ds.SendStringCommand(this.object+" off");
    },

    // Button interface
    //
    setup: function (button, label, button_refs, props) {
        ButtonFx.prototype.setup.call(this, button, label, button_refs, props);
        props.cornerradius = 25;
        if (this.radio_get_status()) {
            props.bordercolor = "30 30 100";
        } else if (this.enable) {
            props.bordercolor = "30 30 100";
            Ds.SendStringCommand(this.object+" on");
            if (this.notes) {
                cp.format_notes({ show: this.show, tag: this.notes }, "current");
            }
        }
    },
    call: function () {
        var status = this.radio_get_status();
        ButtonFx.prototype.call.call(this, ! status);
        if (status) {
            Ds.SendStringCommand(this.object+" off");
            Ds.SetObjectAttrUsingRef(this.button_refs.bordercolor,
                                     { r: 65, g: 65, b: 65 });
            if (this.notes) {
                cp.format_notes(this.show.current_section(true), "current");
            }
        } else {
            Ds.SendStringCommand(this.object+" on");
            Ds.SetObjectAttrUsingRef(this.button_refs.bordercolor,
                                     { r: 30, g: 30, b: 100 });
            if (this.notes) {
                cp.format_notes({ show: this.show, tag: this.notes }, "current");
            }
        }
    },
    cleanup: function () {
        ButtonFx.prototype.cleanup.call(this);
        this.radio_self_off();
    }
};


effect_register('system-toggle', FxSystemToggle);
