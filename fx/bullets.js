

function FxBullets (show, name, object, options) {
    ButtonFx.call(this, show, name, options);
    if (! object) {
        throw "bullets: object name required";
    }
    this.object = object;
}
FxBullets.prototype = {
    constructor: FxBullets,
    __proto__: ButtonFx.prototype,
    object: null,
    toString: function () {
        return "#<FxBullets "+this.object+">";
    },

    // Radio interface
    //
    radio_get_status: function () {
        try {
            return Ds.GetObjectAttr(this.object, "status");
        } catch (e) {
            return 0;
        }
    },

    // Button interface
    //
    setup: function (button, label, button_refs, props) {
        ButtonFx.prototype.setup.call(this, button, label, button_refs, props);
        //XXX: if we're on the last bullet, color the button red
        if (this.radio_get_status()) {
            props.bordercolor = "30 30 100";
        }
    },
    call: function () {
        var status = this.radio_get_status();
        ButtonFx.prototype.call.call(this, ! status);
        if (! status) {
            // turn it on
            Ds.AddObjectChild("scenefixed", this.object);
            Ds.SetObjectAttr(this.object, "status", 1);
            Ds.SetObjectAttr(this.object, "intensity", 100);
            Ds.SetObjectAttrUsingRef(this.button_refs.bordercolor,
                                     { r: 30, g: 30, b: 100 });
        }
        // advance
        var childs = Ds.GetObjectChildNames(this.object);
        childs = childs.sort();
        var i, n = childs.length
        for (i = 0; i < n; ++i) {
            var child = childs[i];
            var child_intensity = Ds.GetObjectAttr(child, "intensity");
            if (child_intensity == 0) {
                Ds.SetObjectAttr(child, "intensity", 100, { total: 1 });
                break;
            }
        }
        if (i == n - 1) {
            // make button red
            Ds.SetObjectAttrUsingRef(this.button_refs.bordercolor,
                                     { r:100, g:0, b:0 });
        } else if (i == n) {
            // turn it off
            Ds.SetObjectAttr(this.object, "status", 0);
            for (i = 0; i < n; ++i) {
                child = childs[i];
                Ds.SetObjectAttr(child, "intensity", 0, { total: 3 });
            }
            var object = this.object;
            var button_refs = this.button_refs;
            sleep(3, function () {
                Ds.RemoveObjectChild("scenefixed", object);
                Ds.SetObjectAttrUsingRef(button_refs.bordercolor,
                                         { r: 65, g: 65, b: 65 });
            });
        }
    },
    cleanup: function () {
        ButtonFx.prototype.cleanup.call(this);
    }
};


effect_register('bullets', FxBullets);
