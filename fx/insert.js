

function FxInsert (show, name, playable, options) {
    ButtonFx.call(this, show, name, options);
    if (! playable) {
        throw "insert: playable required";
    }
    this.playable = playable;
}
FxInsert.prototype = {
    constructor: FxInsert,
    __proto__: ButtonFx.prototype,
    playable: null,
    toString: function () {
        return "#<FxInsert "+this.playable+">";
    },

    // Button interface
    //
    setup: function (button, label, button_refs, props) {
        ButtonFx.prototype.setup.call(this, button, label, button_refs, props);
        props.borderwidth = 4;
    },
    call: function () {
        ButtonFx.prototype.call.call(this, true);
        showstep_insert(this.playable);
    },
    cleanup: function () {
        ButtonFx.prototype.cleanup.call(this);
    }
};


effect_register('insert', FxInsert);
