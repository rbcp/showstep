

function FxAttributeToggle (show, name, object, attribute, options) {
    ButtonFx.call(this, show, name, options);
    if (! object) {
        throw "attribute-toggle: object name required";
    }
    if (! attribute) {
        throw "attribute-toggle: attribute name required";
    }
    this.object = object;
    this.attribute = attribute;
    if ("enable" in options) {
        this.enable = options.enable;
    }
}
FxAttributeToggle.prototype = {
    constructor: FxAttributeToggle,
    __proto__: ButtonFx.prototype,
    object: null,
    attribute: null,
    enable: false,
    toString: function () {
        return "#<FxAttributeToggle "+this.object+"."+this.attribute+">";
    },

    // Radio interface
    //
    radio_get_status: function () {
        try {
            return Ds.GetObjectAttr(this.object, this.attribute);
        } catch (e) {
            return 0;
        }
    },

    // Button interface
    //
    setup: function (button, label, button_refs, props) {
        ButtonFx.prototype.setup.call(this, button, label, button_refs, props);
        props.cornerradius = 25;
        if (this.radio_get_status()) {
            props.bordercolor = "30 30 100";
        } else if (this.enable) {
            props.bordercolor = "30 30 100";
            Ds.SetObjectAttr(this.object, this.attribute, 1);
        }
    },
    call: function () {
        var status = this.radio_get_status();
        ButtonFx.prototype.call.call(this, ! status);
        if (status) {
            Ds.SetObjectAttr(this.object, this.attribute, 0);
            Ds.SetObjectAttrUsingRef(this.button_refs.bordercolor,
                                     { r: 65, g: 65, b: 65 });
        } else {
            Ds.SetObjectAttr(this.object, this.attribute, 1);
            Ds.SetObjectAttrUsingRef(this.button_refs.bordercolor,
                                     { r: 30, g: 30, b: 100 });
        }
    },
    cleanup: function () {
        ButtonFx.prototype.cleanup.call(this);
        Ds.SetObjectAttr(this.object, this.attribute, 0);
    }
};


effect_register('attribute-toggle', FxAttributeToggle);
