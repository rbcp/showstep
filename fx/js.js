
/*

This fx module calls a js script.  The script may be given as an absolute
path beginning with "$", a relative path beginning with "." to be resolved
against the current value of script.play, or a relative path not beginning
with "$" or "." to be resolved against the current value of
script.directory.

Options:

 - once: limit to one play when true

*/

function FxJs (show, name, script, parameters, options) {
    ButtonFx.call(this, show, name, options);
    if (! script) {
        throw "js: script required";
    }
    script = resolve_script(script, null, show.scriptroot);
    this.script = script;
    this.parameters = parameters;
    if ("once" in options) {
        this.once = options.once;
    }
}
FxJs.prototype = {
    constructor: FxJs,
    __proto__: ButtonFx.prototype,
    script: null,
    once: false,
    _played: false,
    toString: function () {
        return "#<FxJs "+this.script+">";
    },

    // Button interface
    //
    call: function () {
        print("js: "+this.script);
        if (! this.once || ! this._played) {
            ButtonFx.prototype.call.call(this, true);
            this._played = true;
            Ds.SetObjectAttrUsingRef(dsrefs.js.play,
                                     { path: this.script,
                                       parameters: this.parameters });
        } else {
            log("This button may only be played once.");
        }
    }
};


effect_register('js', FxJs);
