
// Refs
//
var dsrefs = {
    js: {},
    script: {},
    showstep: {},
    showstep_controlpanel: {},
    system: {}
};

function dsrefs_init () {
    dsrefs.js.play = Ds.NewObjectAttrRef("js", "play");
    dsrefs.script.play = Ds.NewObjectAttrRef("script", "play");
    dsrefs.script.directory = Ds.NewObjectAttrRef("script", "directory");
    dsrefs.script.jsdir = Ds.NewObjectAttrRef("script", "jsdir");
    dsrefs.script.modeldir = Ds.NewObjectAttrRef("script", "modeldir");
    dsrefs.script.audiodir = Ds.NewObjectAttrRef("script", "audiodir");
    dsrefs.script.videodir = Ds.NewObjectAttrRef("script", "videodir");
    dsrefs.script.imagedir = Ds.NewObjectAttrRef("script", "imagedir");
    dsrefs.showstep.button = Ds.NewObjectAttrRef(showstep_dsob, "button");
    dsrefs.showstep.call = Ds.NewObjectAttrRef(showstep_dsob, "call");
    dsrefs.showstep.debug = Ds.NewObjectAttrRef(showstep_dsob, "debug");
    dsrefs.showstep.insert = Ds.NewObjectAttrRef(showstep_dsob, "insert");
    dsrefs.showstep.lock = Ds.NewObjectAttrRef(showstep_dsob, "lock");
    dsrefs.showstep.play = Ds.NewObjectAttrRef(showstep_dsob, "play");
    dsrefs.showstep.world = Ds.NewObjectAttrRef(showstep_dsob, "world");
    dsrefs.showstep.scene = Ds.NewObjectAttrRef(showstep_dsob, "scene");
    dsrefs.showstep.acceptinsert = Ds.NewObjectAttrRef(showstep_dsob, "acceptInsert");
    dsrefs.showstep.acceptnext = Ds.NewObjectAttrRef(showstep_dsob, "acceptNext");
    dsrefs.showstep.status = Ds.NewObjectAttrRef(showstep_dsob, "status");
    dsrefs.system.message = Ds.NewObjectAttrRef("system", "message");
    dsrefs.system.time = Ds.NewObjectAttrRef("system", "time");
}
