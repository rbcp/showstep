
/*
 * Utils: general
 */

function gensym (prefix, dsclass) {
    var i = 0;
    while (true) {
        var tryname = prefix + i;
        var id = Ds.GetObjectID(tryname);
        if (id < 0) {
            Ds.CreateObject(tryname, dsclass);
            return tryname;
        } else {
            i++;
        }
    }
}

function pp (o) {
    for (var k in o) {
        print(k+": "+o[k]);
    }
}

function method_caller (o, method) {
    return function () { o[method](); };
}

function construct (constructor, args) {
    function f () {
        return constructor.apply(this, args);
    }
    f.prototype = constructor.prototype;
    return new f();
}


/*
 * Utils: pathnames
 */

function drop_filename (pathname) {
    var slash = pathname.lastIndexOf('/');
    var backslash = pathname.lastIndexOf('\\');
    return pathname.substr(0, Math.max(slash, backslash)+1);
}

function drop_path_component (pathname) {
    var sp = pathname.split(/[\\/]+(?=[^\\/])/);
    sp.pop();
    return sp.join("/");
}

function take_path_component (pathname) {
    var sp = pathname.split(/[\/\\]+(?=[^\\/])/);
    var c = sp.pop();
    var slash = c.lastIndexOf('/');
    var backslash = c.lastIndexOf('\\');
    var sb = Math.max(slash, backslash);
    if (sb > -1) {
        return c.substr(0, sb);
    } else {
        return c;
    }
}

function take_filename (pathname) {
    var slash = pathname.lastIndexOf('/');
    var backslash = pathname.lastIndexOf('\\');
    var filename = pathname.substr(Math.max(slash, backslash)+1);
    return filename;
}

function take_basename (pathname) {
    var filename = take_filename(pathname);
    var dot = filename.lastIndexOf('.');
    var basename = filename.substr(0, dot);
    return basename;
}

function join_paths (a, b) {
    var alast = a[a.length-1];
    var bfirst = b[0] || "";
    if (bfirst == ".") {
        var bsp = b.split(/[\/\\]/);
        var adrop = 0;
        var bskip = 0;
        for (var i = 0, n = bsp.length; i < n; ++i) {
            if (bsp[i] == ".") {
                bskip++;
            } else if (bsp[i] == "..") {
                bskip++;
                adrop++;
            } else {
                break;
            }
        }
        if (adrop > 0) {
            if (alast == "\\" || alast == "/") {
                adrop++;
            }
            var asp = a.split(/[\\\/]/);
            var aspn = asp.length;
            asp.splice(aspn - adrop, adrop);
            a = asp.join("/");
        }
        bsp.splice(0, bskip);
        b = bsp.join("/");
        bfirst = b[0] || "";
    }
    alast = a[a.length-1];
    if (alast == "\\" || alast == "/" || bfirst == "\\" || bfirst == "/") {
        var sep = "";
    } else {
        sep = "/";
    }
    return a + sep + b;
}

function clean_path (pathname) {
    return pathname.replace(/[\\/]+/g, "\\");
}

function file_readable (pathname) {
    return (Ds.FileAccess(pathname, 4) == 0);
}

function file_read_json (pathname, notfound) {
    try {
        var content = Ds.FileReadAllText(pathname, "r");
        if (content == -1) {
            if (notfound === undefined) {
                throw new Error("Error reading file: "+path);
            } else {
                return notfound;
            }
        }
        return JSON.parse(content);
    } catch (e) {
        if (notfound === undefined) {
            throw e;
        } else {
            return notfound;
        }
    }
}


/*
 * Utils: arrays
 */

/**
 * array_p returns true if its argument is an array, otherwise false.
 */
function array_p (ob) {
    return ob && ob.__proto__ == Array.prototype || false;
}

/**
 * make_array returns its argument unchanged if it is already an array, an
 * empty array if its argument is undefined, otherwise an array containing
 * its object as the sole element.
 */
function make_array (ob) {
    if (array_p(ob)) {
        return ob;
    }
    if (ob === undefined) {
        return [];
    }
    return [ob];
}


/*
 * Utils: objects
 */

function merge_defaults (obj) {
    if (! obj) {
        obj = {};
    }
    var n = arguments.length;
    for (var i = 1; i < n; ++i) {
        var o = arguments[i];
        for (k in o) {
            if (! (k in obj)) {
                obj[k] = o[k];
            }
        }
    }
    return obj;
}

function merge_objects (obj) {
    if (! obj) {
        obj = {}
    }
    var n = arguments.length;
    for (var i = 1; i < n; ++i) {
        var o = arguments[i];
        for (k in o) {
            obj[k] = o[k];
        }
    }
    return obj;
}


/*
 * Utils: string
 */

function format_show_title (str) {
    return str;
}

function format_section (section) {
    if (! section) {
        return "";
    } else if (section.parent) {
        return section.parent + ":" + section.tag;
    } else {
        return section.tag;
    }
}

function format_timestamp (t) {
    if (! t) {
        t = new Date();
    }
    var m = t.getMinutes();
    var s = t.getSeconds();
    return (t.getHours() + ":" +
            (m < 10 ? "0" : "") + m + ":" +
            (s < 10 ? "0" : "") + s);
}

function format_lock_duration (s) {
    var m = Math.floor(s / 60.0);
    var s = Math.round(s - m * 60);
    return m + ":" + (s < 10 ? "0" : "") + s;
}


/*
 * Utils: digistar
 */

function object_exists (object) {
    return Ds.GetObjectID(object) > -1;
}

function status (dsob) {
    try {
        return Ds.GetObjectAttr(dsob, "status");
    } catch (e) {
        return false;
    }
}

function get_self_path () {
    return drop_filename(Ds.UnresolvePathName(Ds.GetCurrentScriptPath()));
}

function active_cameras_off (duration, except) {
    if (duration == null) {
        duration = 0;
    }
    if (except == null) {
        except = [];
    }
    except = make_array(except);
    var camera_cid = Ds.GetClassID("cameraClass");
    var domevideo_cid = Ds.GetClassID("domeVideoClass");
    var names = Ds.GetObjectChildNames("dome");
    for (var i = 0, n = names.length; i < n; ++i) {
        var o = names[i];
        var cid = Ds.GetObjectClassID(o);
        if ((cid == camera_cid || cid == domevideo_cid) && except.indexOf(o) < 0) {
            var info = Ds.GetNameInfo(o);
            if (info.objectType.substr(0, 3) != "per") { // ignore permanent and persistent
                var dur = { total: duration, down: duration * 0.3 };
                if (i == 0) {
                    Ds.SetObjectAttr(o, "color", { r: 0, g: 0, b: 0 }, dur);
                } else {
                    Ds.SetObjectAttr(o, "intensity", 0, dur);
                }
                if (cid == domevideo_cid) {
                    Ds.SetObjectAttr(o, "volume", 0, { total: duration });
                }
            }
        }
    }
}


/**
 * resolve_script converts a relative path to an absolute Digistar path by
 * the following rules:
 *
 *  - if it already starts with '$', return it unchanged.
 *  - if it starts with '.', resolve it relative to the value of script.play.
 *  - otherwise, resolve it relative to script.directory.
 *
 * script.play may be overridden by the optional argument relbase, and
 * script.directory may be overridden by the optional argument searchbase.
 */
function resolve_script (path, relbase, searchbase) {
    var firstchar = path.substr(0, 1);
    if (firstchar == '$') {
        return path;
    }
    if (firstchar == '.') {
        var caller = relbase || Ds.GetObjectAttrUsingRef(dsrefs.script.play);
        var caller_path = drop_filename(caller);
        return join_paths(caller_path, path);
    }
    var script_directory = searchbase || Ds.GetObjectAttrUsingRef(dsrefs.script.directory);
    return join_paths(script_directory, path);
}


/**
 * props_struct_to_script_and_call takes a structure like the following
 * and executes it as a Digistar scriptlet.
 *
 *    {
 *      object1: { attr1: val1, ... },
 *      ...
 *    }
 *
 */
function props_struct_to_script_and_call (objects) {
    var script = "";
    for (var o in objects) {
        var props = objects[o];
        for (var k in props) {
            var v = props[k];
            script += o + " " + k + " " + v + "\n";
        }
    }
    if (script != "") {
        Ds.SendStringCommandBuffer(script);
    }
}


function script_directories_reset () {
    Ds.ResetObjectAttrUsingRef(dsrefs.script.directory);
    Ds.ResetObjectAttrUsingRef(dsrefs.script.jsdir);
    Ds.ResetObjectAttrUsingRef(dsrefs.script.modeldir);
    Ds.ResetObjectAttrUsingRef(dsrefs.script.audiodir);
    Ds.ResetObjectAttrUsingRef(dsrefs.script.videodir);
    Ds.ResetObjectAttrUsingRef(dsrefs.script.imagedir);
}


/*
 * Digistar script
 */

/**
 * format_script takes an array of lines and formats them as a Digistar
 * script suitable for passing to Ds.SendScriptCommands.  It prepends a
 * tab to every line (or puts it in between the timestamp and the
 * command), and appends a newline.
 */
function format_script (lines) {
    var script = "";
    var timestamp_re = RegExp('^(\\+?[\\d\\.]*)\\s*(.*)');
    for (var i = 0, n = lines.length; i < n; ++i) {
        var line = lines[i];
        var m = timestamp_re.exec(line);
        var ts = m[1];
        var command = m[2];
        script += ts + (command ? "\t" : "") + command + "\n";
    }
    return script;
}
