
function ControlPanelUI () {
    this.create_buttons();
    this.init_refs();
}
ControlPanelUI.prototype = {
    constructor: ControlPanelUI,
    toString: function () { return "#<ControlPanelUI "+this.controlpanel_dsob+">"; },
    controlpanel_dsob: "showstep_controlpanel",
    notes_max_pages: 6,
    notes_max_lines_per_page: 3,
    playlist_nbuttons: 6,
    playlist_previous_show: 1,
    playlist_scroll: 0,
    refs: null,
    create_buttons: function () {
        for (var i = 1; i <= 12; ++i) {
            Ds.CreateObject("showstep_button" + i, "buttonClass", "permanent");
        }
        for (i = 1; i <= this.playlist_nbuttons; ++i) {
            Ds.CreateObject("showstep_playlist_button" + i, "buttonClass", "permanent");
        }
        Ds.Wait(0);
    },
    init_refs: function () {
        this.refs = {
            title: Ds.NewObjectAttrRef(this.controlpanel_dsob, "title"),
            nextbuttoncolor: Ds.NewObjectAttrRef(this.controlpanel_dsob, "nextbuttoncolor"),
            currentsection: Ds.NewObjectAttrRef(this.controlpanel_dsob, "currentsection"),
            nextsection: Ds.NewObjectAttrRef(this.controlpanel_dsob, "nextsection"),
            status: Ds.NewObjectAttrRef(this.controlpanel_dsob, "status"),
            notes: [],
            notesnext: [],
            buttons: [null], // buttons are indexed from 1
            playlist_buttons: [null]
        };
        for (var i = 1; i <= this.notes_max_pages; ++i) {
            this.refs.notes.push(
                Ds.NewObjectAttrRef(this.controlpanel_dsob, "currentsectionnotes"+i));
            this.refs.notesnext.push(
                Ds.NewObjectAttrRef(this.controlpanel_dsob, "nextsectionnotes"+i));
        }
        for (i = 1; i <= 12; ++i) {
            var o = "showstep_button" + i;
            this.refs.buttons.push({
                bordercolor: Ds.NewObjectAttrRef(o, "bordercolor"),
                borderwidth: Ds.NewObjectAttrRef(o, "borderwidth"),
                color: Ds.NewObjectAttrRef(o, "color"),
                cornerradius: Ds.NewObjectAttrRef(o, "cornerradius"),
                fontsize: Ds.NewObjectAttrRef(o, "fontsize"),
                labelcolor: Ds.NewObjectAttrRef(o, "labelcolor"),
                label: Ds.NewObjectAttrRef(o, "label"),
                oncolor: Ds.NewObjectAttrRef(o, "oncolor"),
                onlabelcolor: Ds.NewObjectAttrRef(o, "onlabelcolor"),
                onbordercolor: Ds.NewObjectAttrRef(o, "onbordercolor"),
                command: Ds.NewObjectAttrRef(o, "command"),
                object: Ds.NewObjectAttrRef(o, "object")
            });
        }
        for (i = 1; i <= this.playlist_nbuttons; ++i) {
            var o = "showstep_playlist_button" + i;
            this.refs.playlist_buttons.push({
                bordercolor: Ds.NewObjectAttrRef(o, "bordercolor"),
                borderwidth: Ds.NewObjectAttrRef(o, "borderwidth"),
                color: Ds.NewObjectAttrRef(o, "color"),
                cornerradius: Ds.NewObjectAttrRef(o, "cornerradius"),
                fontsize: Ds.NewObjectAttrRef(o, "fontsize"),
                labelcolor: Ds.NewObjectAttrRef(o, "labelcolor"),
                label: Ds.NewObjectAttrRef(o, "label"),
                oncolor: Ds.NewObjectAttrRef(o, "oncolor"),
                onlabelcolor: Ds.NewObjectAttrRef(o, "onlabelcolor"),
                onbordercolor: Ds.NewObjectAttrRef(o, "onbordercolor"),
                command: Ds.NewObjectAttrRef(o, "command"),
                object: Ds.NewObjectAttrRef(o, "object")
            });
        }
    },
    refresh: function () {
        this.playlist_update();
        this.buttons_init(null);
    },


    update_lock_state: function () {
        var now = Ds.GetObjectAttrUsingRef(dsrefs.system.time);
        if (now >= lock_until) {
            if (the_show.next_section(true) || the_show.current_section(true)) {
                Ds.SetObjectAttrUsingRef(dsrefs.showstep.acceptnext, true);
                var color = { r: 0, g: 100, b: 0 };
            } else {
                Ds.SetObjectAttrUsingRef(dsrefs.showstep.acceptnext, false);
                color = { r: 65, g: 65, b: 65 };
            }
            Ds.SetObjectAttrUsingRef(this.refs.nextbuttoncolor, color);
            if (status_priority == 0) {
                Ds.SetObjectAttrUsingRef(dsrefs.system.message, "Ready");
            }
        }
    },

    update_next: function (show, section) {
        var next_section = show.next_section(true, section);
        if (next_section) {
            Ds.SetObjectAttrUsingRef(this.refs.nextsection,
                                     format_section(next_section));
        } else {
            Ds.SetObjectAttrUsingRef(this.refs.nextsection,
                                     "[no section]");
        }
        this.format_notes(next_section, "next");
    },


    format_notes: function (section, which) {
        var max_chars_per_page = 255;
        var max_lines_per_page = this.notes_max_lines_per_page;
        var max_pages = this.notes_max_pages;
        if (which == "current") {
            var refs = this.refs.notes;
        } else {
            refs = this.refs.notesnext;
        }
        var len = 0;
        var nlines = 0;
        var str = "";
        var refidx = 0;
        var suppress_notes = false;
        if (section) {
            var show = section.show;
            var notes = show.notes[section.tag];
            while (typeof notes == "string") {
                notes = show.notes[notes];
            }
            if (notes) {
                for (var i = 0, n = notes.length; i < n; ++i) {
                    var s = notes[i].replace(/  /g,"\u00a0\u00a0");
                    var slen = s.length;
                    if (len + slen < max_chars_per_page && nlines < max_lines_per_page) {
                        if (nlines > 0) {
                            str += "|";
                            len += 1;
                        }
                        str += s;
                        len += slen;
                        nlines++;
                    } else {
                        // write page, start next page
                        Ds.SetObjectAttrUsingRef(refs[refidx], str);
                        refidx++;
                        str = s;
                        len = s.length;
                        nlines = 1;
                        if (refidx >= max_pages) {
                            print("Could not fit all notes for section "+section.tag);
                            return;
                        }
                    }
                }
            } else if (notes === false) {
                suppress_notes = true;
            }
        }
        if (! suppress_notes) {
            for (; refidx < max_pages; ++refidx) {
                Ds.SetObjectAttrUsingRef(refs[refidx], str);
                str = "";
            }
        }
    },


    // Buttons
    //
    buttons_init: function (section) {
        function button_defaults (b) {
            return {
                "bordercolor": "25 25 25",
                "borderwidth": "1",
                "color": "0 0 0",
                "cornerradius": "5",
                "fontsize": "18",
                "fontweight": "light",
                "labelcolor": "65 65 65",
                "label": "-",
                "oncolor": "0 0 0",
                "onlabelcolor": "65 65 65",
                "onbordercolor": "30 30 100",
                "command": "\"system message showstep_button"+b+"\"",
                "object": "NOT_SET"
            };
        }
        if (section && (section.tag in section.show.runtime_menus)) {
            delete section.show.runtime_menus[section.tag];
        }
        var script = {};
        Ds.AllocObjectArray(showstep_dsob, "button", 13); // clear it out
        for (var i = 1; i <= 12; ++i) {
            var b = "showstep_button"+i;
            script[b] = button_defaults(i);
        }
        props_struct_to_script_and_call(script);
    },


    // Playlist
    //
    playlist_update: function () {
        function script_for_button (n, section, color) {
            if (section) {
                var label = section.tag;
                if (section.autoplay) {
                    label += " (autoplay)";
                }
            } else {
                label = "";
            }
            var b = "showstep_playlist_button"+(n);
            var script = b + " bordercolor 25 25 25\n" +
                b + " borderwidth 1\n" +
                b + " color "+color+"\n" +
                b + " cornerradius 0\n" +
                b + " fontsize 18\n" +
                b + " labelcolor 65 65 65\n" +
                b + " label \""+label+"\"\n" +
                b + " oncolor 0 0 0\n" +
                b + " onlabelcolor 65 65 65\n" +
                b + " onbordercolor 30 30 100\n" +
                b + " command \"system message "+b+"\"\n"; //XXX: use showstep.call?
            return script;
        }

        // start with previous sections
        //
        var cur_show = the_show.current_show();
        var section_index = cur_show.playlist.index;
        var preferred_current_section_button = this.playlist_previous_show -
            this.playlist_scroll;
        var previous_to_show = [];
        var bshow = cur_show;
        var actual_previous_to_show = 0, s = section_index - 1;
        while (actual_previous_to_show < preferred_current_section_button) {
            var section = bshow.playlist.get(s);
            if (! section) {
                if (bshow.parent) {
                    // go up to parent
                    s = bshow.parent.playlist.list.indexOf(bshow);
                    bshow = bshow.parent;
                } else {
                    break;
                }
            } else if (section instanceof Show) {
                bshow = section;
                s = bshow.playlist.length();
            } else {
                previous_to_show.unshift(section);
                ++actual_previous_to_show;
            }
            --s;
        }

        if (actual_previous_to_show < preferred_current_section_button) {
            var button_index_of_current = actual_previous_to_show;
        } else {
            button_index_of_current = preferred_current_section_button;
        }

        var script = "";
        for (var i = 0; i < actual_previous_to_show; ++i) {
            section = previous_to_show[i];
            script += script_for_button(i + 1, section, "0 0 0");
        }

        bshow = cur_show;
        // first time through loop will be current section
        if (section_index > -1) {
            var current_p = true;
            var color = "25 25 0";
            s = section_index;
        } else {
            current_p = false;
            color = "0 0 0";
            s = 0;
        }

        for (var i = button_index_of_current;
             i < this.playlist_nbuttons;
             ++i, ++s)
        {
            // do we need to go up the show tree?
            section = bshow.playlist.get(s);
            while (! section && bshow.parent) {
                s = bshow.parent.playlist.list.indexOf(bshow) + 1;
                bshow = bshow.parent;
                section = bshow.playlist.get(s);
            }
            // now we may need to descend into child shows
            while (section instanceof Show) {
                bshow = section;
                s = 0;
                section = bshow.playlist.get(s);
            }
            if (i >= 0 && i < this.playlist_nbuttons) {
                script += script_for_button(i + 1, section, color);
            }
            if (current_p) {
                current_p = false;
                color = "0 0 0";
            }
        }

        Ds.SendStringCommandBuffer(script);
    },

    // Playlist Scrolling
    //
    playlist_scroll_reset: function () {
        this.playlist_scroll = 0;
        this.playlist_update();
    },

    playlist_scroll_down: function () {
        var first_section = Math.max(0, the_show.playlist.index -
                                     this.playlist_previous_show) +
            this.playlist_scroll;
        if (the_show.playlist.length() - first_section >
            this.playlist_nbuttons)
        {
            this.playlist_scroll += 1;
        }
        this.playlist_update();
    },

    playlist_scroll_up: function () {
        var first_section = Math.max(0, the_show.playlist.index -
                                     this.playlist_previous_show) +
            this.playlist_scroll;
        if (first_section > 0) {
            this.playlist_scroll -= 1;
        }
        this.playlist_update();
    }


};



function file_explorer_on_show () {
    var s = the_show.current_show();
    if (s) {
        var path = Ds.ResolvePathName(s.showroot);
        Ds.SystemCommand("explorer "+path);
    } else {
        print("error: no current show");
    }
}
