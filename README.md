
# ShowStep

Note - this project is deprecated.  Please see showstep5 instead.  This project, Showstep3, now works as an effect within Showstep5, but will eventually be entirely replaced by Showstep5.

## Overview

ShowStep is a show production and presentation framework for the Digistar planetarium system.  It is built around the following design goals:

  - Live narrated, presenter controlled and timed shows.
  - Simple control panel interface that prevents common mistakes like a wrong button press or accidental double-click.
  - Continuous visuals without the need to fade and reset between scenes.
  - Promotes conventions for good and consistent organization of scripts and assets.
  - Coding conventions that reduce boilerplate and facilitate copy & paste between editor and Digistar prompt.
  - One control panel page, many shows.
  - Presenter notes or script right in the control panel.
  - Ability to start a show in the middle.
  - Sections are self-contained and do not need to know internal details of other sections.


## Main Concepts

### Show, Playlist, Section

A _show_ is a _playlist_ of _sections_.  The playlist may be predetermined in the scripting of the show, or spliced together on the fly while presenting.  The playlist is a flat list, but sections may also have hierarchical relationships, which determines order and timing of cleanup phases.

The normal run of a simple show consists of:

 * A call to `showstep play <playable>`
 * Repeated calls to `showstep next` to advance the show.


### Section, Effect, Scene

Sections are the backbone of a show, while effects and scenes are two other mechanisms to help you organize your show and develop it efficiently.

Think of sections like the outline of your presentation.  They are the concepts you will be talking about, and they have a foreward progression.

Effects are, generally speaking, add-ins, things that turn on and off, do and undo.  They do not make a lasting change to the system state.  Effects can be triggered by the presenter via buttons, or automatically as the show advances.

Scenes - not to be confused with Digistar's sceneClass (sorry!) - are like a level in a video game - the basic collection of objects and mechanisms for manipulating them.  Between two calls to `system reset`, the objects needed by your presentation comprise the scene.  A scene has nodes: known states of those objects; and transitions: how to get from one node to another.  This graph can be as simple or as elaborate as you like, and ShowStep can partially automate moving through it as you advance sections.  These known states also provide a way to create a show that can be started from any section.


### Playable ###

A _playable_ is a string that designates a show or a playable section within a show.  Playables are used to start shows and insert new sections into a running show.  They can be any of the following forms:

 * A path to a .show file, with optional `|section`.
 * The path to a show root directory with required `|section`.
 * The path to a section script in a file-based playlist.
 * The path to a section script in a file-based playlist, with the section tag in place of the filename.
 * The path of a `-play` script of a standalone section.
 * The path of a standalone section directory.  The directory must contain a file with the same name as the directory plus "-play.ds".

As a special case for `showstep play`, playable can also be:

 * The tag (name) of a section in the current playlist.  If the given section has a -setup script, the show will be restarted from that section.


### Phases of a Show ###

A section has a _play_ phase, a _cleanup_ phase, and optionally, a _setup_ phase.



## Compatibility

ShowStep is currently compatible with Digistar 6 6.18.08.2 and higher.


## Installation

Download or clone the showstep repository and extract it to the desired location, for example, `$Content/User/js/showstep`.

ShowStep may be started by the particular presentations that use it, or you can install it such that it will always be running in the background.


### Run in Background

Add the following clause to both `$Content/User/startup.ds` and `$Content/User/fadestopreset.ds`, adjusting the path as needed for your local installation.

```
## showstep
##
script play $Content/User/js/showstep/showstep.ds
```


## Commands
### Play

```
showstep play $Content/path/to/00.a-section.ds
```

 1. Load scripts/show-notes.json
 1. Discover other sections associated this script.
 1. Stop all running scripts.
 1. Cancel active locks and pending actions.
 1. Fade all cameras and domevideo.
 1. System reset.
 1. Call section setup script if it exists. (Required for any but first section.)
    1. Automatic 1 second lock.
    1. Setup script may lock for more time if it needs to.
 1. Play section.
    1. Automatic 1 second lock.
    1. Fill control panel.
    1. Initialize programmable buttons.
    1. Set script.directory & friends. (See *Script.directory & Friends* below.)
    1. Script.play section script, which may lock for more time.


## Setup and Cleanup

Setup and cleanup scripts associated with a section get special treatment.  ShowStep looks for them based on their filename.  For example, with a section called 'a-section', ShowStep will look for *scripts/a-section/a-section-setup.ds* and *scripts/a-section/a-section-cleanup.ds*.

The setup script is used with the 'play' command.  After performing a system reset, the setup script's job is to setup the scene however it is needed for that section.  Normally, this is only used during show development, so you want this script to do its job as quickly as possible.

The cleanup script is used with the 'next' command.  When the show advances to the next section, the cleanup script is called to garbage collect any resources used by the current section.

Both setup and cleanup scripts get an automatic 1 second lock to do what they need to do.  However, if they need more time, they can call `showstep lock` with a given duration (in seconds) to extend the lock.  ShowStep will not advance to the next section until after the lock is finished.


## Script.directory & Friends

Any time you play a script with showstep, it automatically sets the following values:

 - script.directory: <show>/scripts
 - script.jsdir: <show>/scripts
 - script.modeldir: <show>/models
 - script.audiodir: <show>/audio
 - script.videodir: <show>/video
 - script.imagedir: <show>/images

If you structure your show to follow this convention and use these directories, it makes development convenient.  It means that you can use paths like this:

```
script include a-section/a-section-define.ds
```

This has less clutter than an absolute path, and is more flexible with copy-and-paste between Digistar and an editor than a relative path.


## Button FX
### Scripted-toggle

```
showstep button 1 'My|Toggle',fx('scripted-toggle','my_object','$Content/path/to/my-object-on.ds')
```

The scripted-toggle fx manages a toggle button for an object that it turned on by a script.  An -off script and/or a -define script may also optionally be provided.  In the fx call when defining the button, you supply the name of the -on script, and this module will automatically check for the existence of the other scripts by substituding '-on' for '-off' and '-define' in the filename.

The -on script may be given as an absolute path, or, if it does not begin with '$' or '.' it will be resolved against script.directory.

The 'status' attribute of the object is used to determine whether the object is on or off, so the -on script should set status on, and the -off script (if present) should set status off or delete the object.

If the '-define' script exists, it is called automatically when the button is initially setup.  Its purpose is for toggles that may require additional load time for media.  They should define the object and set the object's status to off.

The behavior of this fx with its different possible uses are as follows.

 - "-on" only:

   When toggling the object off, the object intensity fades to 0 over 3 seconds, then the object is deleted. (XXX: to-do: and its children?)

 - "-on" and "-off" only:

   When toggling the object off, the -off script is called.  The -off script should set status off or delete the object.

 - "-on" and "-define" only:

   When toggling the object off, the object intensity fades to 0 over 3 seconds, then the object's status is set to off.

 - "-on", "-off", and "-define":

   Same as for "-on" and "-off" only.  The -off script should set the object's status to off, and not delete it.


## Show Notes

Every section of a show can have presenter notes that will appear in the ShowStep control panel page while running the show.

Notes should be an array of strings.  Up to 18 lines can be fit into the control panel.  ShowStep will perform some rudimentary line wrapping for lines that are too long.

Notes can also be the value `false`, in which case the notes will not update when advancing to the section.  This is useful for inserted effects that do not have notes of their own.

The notes can be stored in three places:

 - The .show file:
 
   For shows defined with a .show file, this is the easiest way to do the notes - they are right in the same file as the playlist and section properties.  They can be in the playlist as the "notes" key in a playlist entry, or they can be in the "notes" section, a key-value map of section names as keys and notes as values.
   
 - The show-notes.json file:
 
   For shows that use the scripts/*.ds filename playlist convention, the show notes go into scripts/show-notes.json.  The contents are a json key-value object, where section names are the keys and the values are the notes.


### Precedence

Because the show notes can be stored in several places, there is the question of precedence.  Here is the precedence from highest to lowest:

 - .show file, playlist entry "notes" key
 - .show file, "notes" section
 - scripts/show-notes.json


## Playing a Show

When we refer to a 'playable', we mean a string that refers to the path to a show, or to a playable section within a show.

### On Startup

When ShowStep is started, a playable may optionally be passed to the 'start' procedure.  ShowStep will then start that show immediately.  This is intended for when ShowStep is used for a standalone show, and is not installed to automatically run in the background.


### Via 'showstep play'

ShowStep fades all active cameras, calls 'system reset' and initializes its internal state including its playlist.

## Rationales
### Precedence of properties

Properties can come from a .show file, and in the case of notes, scripts/show-notes.json.

The show-notes.json file is overridden by the .show file.


## Version History

* **0.1.0**  
'Orchestrate', originally developed for a show called _Subatomic Live_.  
Playlists defined as a numbered sequence of script files.
* **0.2.0**  
'Orchestrate' developed further for a show called _Star Lore_.
* **1.0.0** (2017-12-24)  
Project named ShowStep (thanks for the name, Waylena!)
* **1.1.0** (2018‑03‑19)  
Hierarchical topic system introduced.
* **1.2.0** (2018‑06‑26)  
Button-fx introduced.
* **1.3.0** (2018‑07‑26)  
Standalone sections introduced.
* **1.4.0** (2018‑07‑27)  
`showstep insert` command introduced.
* **2.0.0** (2018‑10‑27)  
Json `.show` format introduced.
* **2.1.0** (2018‑12‑13)  
`require` introduced.
* **2.2.0** (2019‑04‑04)  
Portability improvements.
* **2.3.0** (2019‑05‑28)  
Generalized effects system.
* **2.4.0** (2019‑08‑27)  
Scenes introduced.
* **2.5.0** (2019‑11‑09)  
Menus may be defined in the .show file.
* **2.5.1** (2020‑03‑14)  
Versioned releases introduced.
* **2.5.2** (2020-03-23)  
bugfix
