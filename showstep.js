
var showstep_version = "3.1.0-alpha";


// Configuration
//
var showstep_dsob = "showstep";

// Internal globals
//
var router; // event router
var self_path;
var the_show;
var cp; // control panel ui
var lock_until = 0;
var timers = []; // queue of sleeping processes
var last_called_script;
var fx_load_path = null;
var effect_modules = {};
var fx_counter = 0;
var status_priority = 0; // 0 means an internal message, >0 means script message

include("./src/refs.js");
include("./src/utils.js");
include("./src/ui.js");


/**
 * Section
 */

function Section (show, spec) {
    this.show = show;

    // High precedence properties
    //
    if (! ("section" in spec)) {
        throw new Error("'section' key missing in playlist entry");
    }
    if (! (typeof spec.section == "string") || ! spec.section) {
        throw new Error("'section' key must be a non-null string");
    }
    this.tag = spec.section;
    var tag_components = this.tag.split(".");
    var ntag_components = tag_components.length;
    var taglast = tag_components[ntag_components - 1];
    if ("script_play" in spec) {
        var play_script_filename = spec.script_play;
    } else {
        play_script_filename = taglast + "-play.ds";
    }
    if ("script_setup" in spec) {
        var setup_script_filename = spec.script_setup;
    } else {
        setup_script_filename = taglast + "-setup.ds";
    }
    if ("script_cleanup" in spec) {
        var cleanup_script_filename = spec.script_cleanup;
    } else {
        cleanup_script_filename = taglast + "-cleanup.ds";
    }
    if ("dir" in spec) {
        var dir = spec.dir;
    } else {
        dir = join_paths(show.scriptroot, tag_components.join("/"));
    }
    if ("play_script" in spec) {
        var play_script = spec.play_script;
    } else {
        play_script = clean_path(join_paths(dir, play_script_filename));
    }
    this.dir = dir; // helper directory
    this.play_script = play_script; // full path
    this.script_play = play_script_filename; // just the filename
    this.script_setup = setup_script_filename;
    this.script_cleanup = cleanup_script_filename;
    this.cleanup_hook = [];
    merge_defaults(this, spec);

    // Low precedence properties
    //
    merge_defaults(this, {
        setup_script: join_paths(dir, setup_script_filename),
        cleanup_script: join_paths(dir, cleanup_script_filename),
        setup_override_play: false,
        lock: 1,
        lock_cleanup: 1,
        lock_setup: 1,
        accept_insert: false,
        autoadvance: false,
        autoplay: false,
        plugin: null,
        notes: null
    });

    // Property post-processing: World
    //
    if (this.world) {
        this.world = show.world_node_get_create(this.world);
    } else if (this.scene) { // backwards compat with < 3.0.0
        this.world = show.world_node_get_create(this.scene);
        delete this.scene;
    }

    // Property post-processing: Requires
    //
    this.use_effects = {};
    var requires = this.require;
    if (requires) {
        for (var i = 0, n = requires.length; i < n; ++i) {
            var k = requires[i];
            if (k[0] == "@" || k.indexOf(":") > -1) { // backref or effect-spec
                var e = effect_spec_parse(show, k);
            } else {
                // need to construct it and store the ref
                var rbasename = take_filename(k);
                var ron_script_name = rbasename + "-on.ds";
                var ron_script_path = join_paths(k, ron_script_name);
                var rspec = "scripted-toggle:FIXME:"+ron_script_path+
                    ":name="+k+":enable=true";
                e = effect_spec_parse(show, rspec);
            }
            e.require_p = true;
            show.effects[e.name] = e;
            this.use_effects[e.name] = true;
        }
    }
}
Section.prototype = {
    constructor: Section,
    toString: function () { return "#<Section "+this.tag+">"; },
    tag: null,
    dir: null,
    play_script: null, // full path
    script_play: null, // just filename
    show: null,
    cleanup_hook: null,
    use_effects: null,

    // section_spec_from_file_playlist_match takes a regexp match object
    // for a file-playlist style section script and returns an object
    // suitable for passing to new Section. It should be called
    // statically.
    section_spec_from_file_playlist_match: function (spec) {
        // spec is a regexp match object
        var ord = parseFloat(spec[1]);
        var tag = spec[2];
        var play_script = clean_path(spec[0]);
        var play_script_filename = take_filename(play_script);
        // split tag into components on ".".  the components make up the
        // section's directory path, and the final component is also the
        // filename prefix for -setup, -cleanup, etc, helper scripts.
        var tag_components = tag.split(".");
        var ntag_components = tag_components.length;
        var taglast = tag_components[ntag_components - 1];
        //var dir = join_paths(show.scriptroot, tag_components.join("/"));
        var section_spec = {
            section: tag,
            ord: ord, // needed for sorting filenames
            play_script: play_script,
            script_play: play_script_filename
        };
        return section_spec;
    },

    // section_spec_from_standalone_script_path takes a play_script pathname
    // and a tag, and returns an object suitable for passing to new Section.
    // It should be called statically.
    section_spec_from_standalone_script_path: function (play_script, tag) {
        var section_spec = {
            section: tag,
            play_script: play_script,
            script_play: take_filename(play_script),
            dir: drop_filename(play_script)
        };
        return section_spec;
    }
};


/**
 * Playlist
 *
 *   Manages a list of sections and nested shows, but does not deal with
 * internal details of those things. The 'show' class handles the nesting.
 */

function Playlist (show, list, index) {
    this.show = show;
    this.list = (list ? list : []);
    this.index = (typeof index == 'number' ? index : -1);
}
Playlist.prototype = {
    constructor: Playlist,
    toString: function () { return "#<Playlist ("+this.length()+")>"; },
    show: null,
    list: null,
    index: -1,
    length: function () {
        return this.list.length;
    },
    current: function () {
        return this.list[this.index] || null;
    },
    next: function () {
        return this.list[this.index + 1] || null;
    },
    get: function (i) {
        return this.list[i] || null;
    },
    find: function (pred) {
        return this.list.find(pred) || null;
    },
    find_index: function (pred) {
        return this.list.findIndex(pred);
    },
    insert: function (i, item) {
        if (item instanceof Show) {
            item.parent = this.show;
        }
        this.list.splice(i, 0, item);
    }
};


/**
 * Show takes an absolute or relative Digistar-style file path, and
 * attempts to resolve the playlist and section to play.  Its return value
 * is a Show object containing the playlist that was found, with the index
 * set to the entry to play.  It does not check whether the given section
 * actually has a valid world or setup script - that must be checked by
 * the caller.
 *
 *  - relative paths that start with '.' are relative to script.play
 *  - relative paths that do not start with '.' are relative to script.directory.
 *
 * The preferred way to create a showstep show is to write a .show file
 * and pass its path to this constructor.  However, for backward
 * compatibility the following are all supported:
 *
 *  - the filepath of a section script.
 *  - the filepath of the scripts directory joined with the    --XXX deprecate this
 *    tag of the section to play.
 *  - the filepath of the root show directory followed by '|section'.
 *    section scripts must be in the 'scripts' subdirectory.
 *  - the filepath of a .show file in the showroot of a show,
 *    optionally followed by '|section'.
 *  - the filepath of a -play script (a standalone section).
 *  - the filepath of a directory that contains a -play script with
 *    the same prefix as the directory name (a standalone section).
 *
 * If no playable is passed to this constructor, a Show with an empty
 * playlist is return.
 */
function Show (playable) {
    if (playable) {
        var pipe = playable.indexOf('|');
        if (pipe > -1) {
            var playable_section_name = playable.substr(pipe + 1);
            playable = playable.substr(0, pipe);
        }
        var resolved = resolve_script(playable);
        var path_last = take_path_component(resolved);
        var maybe_play_script = join_paths(resolved, path_last+"-play.ds");
        if (resolved.slice(-5) == ".show") { // show file
            this.source = resolved;
            this.path = resolved;
            this.spec = file_read_json(resolved);
            if ("showroot" in this.spec) {
                this.showroot = resolve_script(this.spec.showroot, this.path);
            } else {
                this.showroot = drop_filename(this.path);
            }
        } else if (resolved.slice(-8) == "-play.ds") { // standalone
            this.source = resolved;
            this.path = resolved;
            this.spec = this._make_spec_from_standalone(resolved);
            this.showroot = drop_filename(resolved);
        } else if (file_readable(maybe_play_script)) { // standalone
            this.source = resolved;
            this.path = resolved;
            this.spec = this._make_spec_from_standalone(maybe_play_script);
            this.showroot = resolved;
        } else { // script sequence
            // generate a .show json spec from the filepath information
            // given.  This ensures all show parsing goes through the same
            // route.
            this.source = resolved;
            this.path = resolved;
            this.spec = this._make_spec_from_script_sequence(
                resolved, playable_section_name);
            this.showroot = this.spec.showroot;
        }
        this._construct_from_show_spec();
        if (playable_section_name) {
            // is playable_section_name a key in playlists?
            if (playable_section_name in this.playlists) {
                this.playlist = this.playlists[playable_section_name];
            } else {
                var section_index = this.playlist.find_index(
                    function (x) {
                        return (x.tag == playable_section_name ||
                                x.script_play == playable_section_name);
                    });
                if (section_index < 0) {
                    throw new Error("Section not found: "+playable_section_name);
                }
                this.start_index = section_index;
            }
        }
    } else {
        this.playlist = new Playlist(this);
    }
}
Show.prototype = {
    constructor: Show,
    parent: null,           // parent Show of a nested Show
    source: null,
    path: null,             // path of the .show file
    spec: null,             // json spec for the Show
    showroot: null,         // root directory of the Show
    scriptroot: null,       // root directory of Show's scripts
    worldsroot: null,       // root directory of Show's worlds scripts
    fx_directory: null,     // Show's fx directory (usually scriptroot/fx)
    script_directory: null, // path for script.directory (usually scriptroot)
    script_jsdir: null,     // path for script.jsdir (usually scriptroot)
    script_modeldir: null,  // path for script.modeldir (usually showroot/models)
    script_audiodir: null,  // path for script.audiodir (usually showroot/audio)
    script_videodir: null,  // path for script.videodir (usually showroot/video)
    script_imagedir: null,  // path for script.imagedir (usually showroot/images)
    playlist: null,
    start_index: 0,
    def_playlists: null,    // additional playlists (spec)
    playlists: null,        // additional playlists (constructed)
    effects: null,
    menus: null,
    worlds: null,
    current_world_node: null,
    notes: null,
    autoplay: false,
    def_defaults: null,
    def_menus: null,
    toString: function () {
        return "#<Show "+this.source+">";
    },
    _construct_from_show_spec: function () {
        // inputs: this.spec, this.showroot
        var show_def = this.spec;
        if (! ("playlist" in show_def)) {
            throw new Error("no playlist in show");
        }
        if (! array_p(show_def.playlist)) {
            throw new Error("playlist is not an array");
        }
        if ("dependencies" in this.spec) {
            this.assert_dependencies(this.spec.dependencies);
        }
        if ("scriptroot" in this.spec) {
            this.scriptroot = this.spec.scriptroot;
        } else {
            this.scriptroot = join_paths(this.showroot, "scripts");
        }
        this.fx_directory = join_paths(this.scriptroot, "fx");
        this.def_defaults = show_def.defaults || {};
        var def_playlist = show_def.playlist;
        this.def_menus = show_def.menus || {};
        this.runtime_menus = {};
        var show_notes = file_read_json(
            join_paths(this.scriptroot, "show-notes.json"),
            {});
        this.notes = merge_defaults(show_def.notes, show_notes);
        this.def_playlists = show_def.playlists || {};
        this.worlds = {};
        this.fx_callbacks = {};
        this.autoplay = show_def.autoplay || false;

        // construct effects from effects section
        this._construct_effects(show_def.effects || {});

        // construct sections - 'require' effects will populate. We also
        // parse and populate section menus here.
        //
        this.menus = {};
        this.playlist = this._construct_a_playlist(
            def_playlist, this.def_defaults, this.def_menus);
        print("found "+def_playlist.length+" sections");
        if (this.playlist.length() && this.playlist.get(0).autoplay) {
            this.autoplay = true;
        }

        // construct secondary playlists
        this.playlists = {};
        for (var playlist_name in this.def_playlists) {
            this.playlists[playlist_name] = this._construct_a_playlist(
                this.def_playlists[playlist_name], this.def_defaults,
                this.def_menus);
        }

        // now follow up by constructing any leftover menus from this.def_menus
        // for which there wasn't a section
        //
        for (var k in this.def_menus) {
            var def_menu = this.def_menus[k];
            if (k in this.menus) { // section menu, already done
                continue;
            }
            this.menus[k] = {};
            for (var b in def_menu) {
                var menu_item_spec = def_menu[b];
                var parsed = button_spec_parse(this, menu_item_spec);
                var label = parsed[0];
                var action = parsed[1];
                this.menus[k][b] = [label, action.name];
                // note these leftover menus will need to be put into
                // use_effects for a section whenever they get used.
            }
        }
        var scenes_dir = join_paths(this.scriptroot, "scenes");
        if (file_readable(scenes_dir)) {
            this.worldsroot = scenes_dir; // backwards compat with < 3.0.0
        } else {
            this.worldsroot = join_paths(this.scriptroot, "worlds");
        }
        this.script_directory = this.scriptroot;
        this.script_jsdir = this.scriptroot;
        this.script_modeldir = join_paths(this.showroot, "models");
        this.script_audiodir = join_paths(this.showroot, "audio");
        this.script_videodir = join_paths(this.showroot, "video");
        this.script_imagedir = join_paths(this.showroot, "images");
        this.current_world_node = null;
    },
    assert_dependencies: function (dependencies) {
        for (var i = 0, n = dependencies.length; i < n; ++i) {
            var path = dependencies[i];
            if (! file_readable(path)) {
                print("Missing dependency for show: "+path); // workaround for TinyJS
                throw new Error("Missing dependency for show: "+path);
            }
        }
    },
    _construct_effects: function (effects_spec) {
        this.effects = {};
        for (var effect_name in effects_spec) {
            var effect_spec = effects_spec[effect_name];
            if (effect_spec[0] == "@") {
                throw new Error("Refs are not allowed in effects section: "+
                                effect_spec);
            }
            var action = effect_spec_parse(this, effect_spec,
                                           { name: effect_name });
            //XXX revisit this odd handling of the persist property
            if (! action.hasOwnProperty("persist")) {
                action.persist = true;
            }
            this.effects[effect_name] = action;
        }
    },
    _construct_a_playlist: function (spec, defaults, def_menus) {
        var sections = [];
        for (var i = 0, n = spec.length; i < n; ++i) {
            var section_def = merge_defaults(spec[i], defaults);
            // this populates worlds
            var section = new Section(this, section_def);
            // notes in the section spec override others
            if (section.notes || section.notes === false) {
                this.notes[section.tag] = section.notes;
            }
            // construct the menu for this section (if needed)
            if (! (section.tag in this.menus)) {
                this.menus[section.tag] = {};
            }
            var def_menu = def_menus[section.tag] || {};
            for (var b in def_menu) {
                // if this section tag is duplicate, we just need to set the
                // ref in use_effects
                if (! (b in this.menus[section.tag])) {
                    var menu_item_spec = def_menu[b];
                    var parsed = button_spec_parse(this, menu_item_spec);
                    var label = parsed[0];
                    var action = parsed[1];
                    var actionname = action.name;
                    this.menus[section.tag][b] = [label, actionname];
                } else {
                    var ref = this.menus[section.tag][b];
                    actionname = ref[1];
                }
                section.use_effects[actionname] = true;
            }
            sections.push(section);
        }
        return new Playlist(this, sections);
    },
    _make_spec_from_script_sequence: function (path, playable_section_name) {
        var spec = {};
        if (playable_section_name) {
            var scriptroot = join_paths(path, "scripts");
        } else if (path.slice(-3) == ".ds") {
            scriptroot = drop_filename(path);
        } else {
            scriptroot = join_paths(path, "scripts");
        }
        spec.showroot = drop_path_component(scriptroot);

        var sequence_sections = [];
        var files = Ds.DirectorySearch(scriptroot, "*.ds", false);
        var section_script_re = /^.*[\/\\](\d+(?:\.\d+)?)[-._\s]+([^\/\\]*)\.ds$/;
        for (var i = 0, n = files.length; i < n; ++i) {
            var path = files[i];
            var match = section_script_re.exec(path);
            if (match) {
                var section_spec = Section.prototype.section_spec_from_file_playlist_match(
                    match);
                sequence_sections.push(section_spec);
            }
        }
        sequence_sections = sequence_sections.sort(
            function (a, b) { return a.ord - b.ord; });

        spec.playlist = sequence_sections;

        return spec;
    },
    _make_spec_from_standalone: function (play_script) {
        var filename = take_filename(play_script);
        var tag = filename.substr(0, filename.length - 8); // remove -play.ds
        var scriptroot = drop_filename(play_script);
        sequence_sections = [
            Section.prototype.section_spec_from_standalone_script_path(play_script, tag)
        ];
        var show_spec = {
            scriptroot: scriptroot,
            playlist: sequence_sections
        };
        return show_spec;
    },
    current_show: function () {
        // the show object corresponding to the current section
        var s = this;
        var t;
        while ((t = s.playlist.current()) instanceof Show) {
            s = t;
        }
        return s;
    },
    current_section: function (recurse) {
        // may return a Show if recurse is falsey
        if (recurse) {
            var s = this;
            var t;
            while ((t = s.playlist.current()) instanceof Show) {
                s = t;
            }
            return t;
        } else {
            return this.playlist.current();
        }
    },
    next_section: function (recurse, from) {
        if (recurse) {
            if (from instanceof Show) {
                var startindex = this.playlist.list.indexOf(from);
            } else if (from) {
                startindex = this.playlist.list.indexOf(from) + 1;
            } else if (this.current_section() instanceof Show) {
                startindex = this.playlist.index;
            } else {
                startindex = this.playlist.index + 1;
            }
            for (var i = startindex, n = this.playlist.length(); i < n; ++i) {
                var item = this.playlist.get(i);
                if (item instanceof Show) {
                    var child = item.next_section(true);
                    if (child) {
                        return child;
                    }
                } else {
                    return item;
                }
            }
            return null;
        } else if (from) {
            return this.playlist.get(this.playlist.index + 1);
        } else {
            return this.playlist.next();
        }
    },
    walk_forward: function (recurse, from_section, fn) {
        //XXX once we can depend on chakracore, this can be turned into an iterator
        var bshow = from_section.show;
        var s = bshow.playlist.list.indexOf(from_section);
        for (; ; ++s) {
            // do we need to go up the show tree?
            var section = bshow.playlist.get(s);
            if (recurse) {
                while (! section && bshow.parent) {
                    s = bshow.parent.playlist.list.indexOf(bshow) + 1;
                    bshow = bshow.parent;
                    section = bshow.playlist.get(s);
                }
                // now we may need to descend into child shows
                while (section instanceof Show) {
                    bshow = section;
                    s = 0;
                    section = bshow.playlist.get(s);
                }
            }
            if (section) {
                if (! fn(section)) {
                    return;
                }
            } else {
                return;
            }
        }
    },
    walk_backward: function (recurse, from_section, fn) {
        //XXX once we can depend on chakracore, this can be turned into an iterator
        if (from_section) {
            var bshow = from_section.show;
            var section_index = bshow.playlist.list.indexOf(from_section);
        } else {
            bshow = this;
            section_index = this.playlist.length();
        }
        var s = section_index - 1;
        while (true) {
            var section = bshow.playlist.get(s);
            if (! section) {
                if (recurse && bshow.parent) {
                    // go up to parent
                    s = bshow.parent.playlist.list.indexOf(bshow);
                    bshow = bshow.parent;
                } else {
                    break;
                }
            } else if (recurse && section instanceof Show) {
                bshow = section;
                s = bshow.playlist.length();
            } else {
                if (! fn(section)) {
                    return;
                };
            }
            --s;
        }
    },
    get_section_by_tag: function (tag) {
        var s = this.playlist.find(function (x) { return x.tag == tag; });
        if (s) {
            return s;
        } else {
            for (var p in this.playlists) {
                var playlist = this.playlists[p];
                s = playlist.find(function (x) { return x.tag == tag; });
                if (s) {
                    return s;
                }
            }
            return null;
        }
    },
    set_asset_directories: function () {
        if (this.showroot) {
            var script = "script directory "+this.script_directory+"\n"+
                "script jsdir "+this.script_jsdir+"\n"+
                "script modeldir "+this.script_modeldir+"\n"+
                "script audiodir "+this.script_audiodir+"\n"+
                "script videodir "+this.script_videodir+"\n"+
                "script imagedir "+this.script_imagedir+"\n";
            Ds.SendStringCommandBuffer(script);
        }
    },
    world_node_get_create: function (nodespec) {
        var sepi = nodespec.indexOf(':');
        if (sepi > -1) {
            var name = nodespec.substr(0, sepi);
            var node = nodespec.substr(sepi + 1);
        } else {
            name = nodespec;
            node = null;
        }
        if (! (name in this.worlds)) {
            this.worlds[name] = new World(this, name);
        }
        var world_with_node = { __proto__: this.worlds[name],
                                node: node };
        return world_with_node;
    },
    populate_fx_callbacks: function () {
        this.fx_callbacks = {};
        for (var name in this.effects) {
            var r = this.effects[name];
            if (r instanceof ButtonFx) {
                this.fx_callbacks[r.name] = r;
            }
        }
        //XXX we will need to recurse into child shows
    },
    cleanup_list_for_section: function (section) {
        // section may be null if we are advancing off the end of the playlist
        // section can also be null if index is -1
        if (section) {
            var start_index = this.playlist.list.indexOf(section) - 1;
        } else {
            section = { tag: "" };
            start_index = this.playlist.length() - 1;
        }
        var index = start_index;
        var tag = section.tag;
        var topic = tag.split(".");
        var topic_nparts = topic.length;
        var sections = [];
        var nsections = 0;
        var nchildsections = 0;
        var cleanup_parts = 100;

        // do we need to clean up a previous sibling show?
        //XXX this is pretty hacky
        //
        var maybe_prev_sibling_show = null;
        var prev_sibling_show_cleanup_list = [];
        if (section && section.show /* not our dummy section hack */) {
            var _this = this; // for walk_backward callback closure
            this.walk_backward(true, section, function (x) {
                // look for sibling show
                if (x.show != _this && x.show.parent == _this.parent) {
                    maybe_prev_sibling_show = x.show;
                }
                return false;
            });
            if (maybe_prev_sibling_show) {
                prev_sibling_show_cleanup_list =
                    maybe_prev_sibling_show.cleanup_list_for_section(null);
            }
        }

        for (index = start_index; index >= 0; index--) {
            var earlier = this.playlist.get(index);
            if (earlier instanceof Show) {
                // we only need to clean it up if it's the first entry we encounter while counting back
                if (index == start_index) {
                    var child_cleanup_sections = earlier.cleanup_list_for_section(null);
                    sections = sections.concat(child_cleanup_sections);
                    nchildsections = child_cleanup_sections.length;
                }
                continue;
            }
            var earlier_topic = earlier.tag.split(".");
            var earlier_topic_nparts = earlier_topic.length;
            var i;
            for (i = 0; i < earlier_topic_nparts; ++i) {
                if (i >= topic_nparts) { // previous topic longer
                    break;
                }
                if (earlier_topic[i] != topic[i]) { // previous topic different
                    break;
                }
            }
            // i now has the index of the first difference
            if (i == earlier_topic_nparts) {
                // topic is a child of previous topic; nothing to clean up.
                break;
            } else if (earlier_topic_nparts < cleanup_parts) {
                cleanup_parts = earlier_topic_nparts;
                if (nsections > 0) {
                    var pfx = sections[nchildsections + nsections - 1].section.tag.substr(
                        0, earlier.tag.length + 1);
                    if (earlier.tag + "." != pfx) {
                        // if the the last item pushed to sections is not a prefix
                        // of the section under examination, it does not need to
                        // be cleaned up, but continue the search for ancestors.
                        continue;
                    }
                }
                sections.push(new SectionCleanupEvent(earlier));
                nsections++;
            }
        }
        return prev_sibling_show_cleanup_list.concat(sections);
    },
    effects_off_list_for_section: function (section) {
        if (section) {
            var section_index = this.playlist.list.indexOf(section);
        } else {
            section_index = this.playlist.length();
        }

        // do we need to clean up a previous sibling show?
        //XXX this is pretty hacky
        //
        var maybe_prev_sibling_show = null;
        var prev_sibling_show_cleanup_list = [];
        if (section) {
            var _this = this; // for walk_backward callback closure
            this.walk_backward(true, section, function (x) {
                // look for sibling show
                if (x.show != _this && x.show.parent == _this.parent) {
                    maybe_prev_sibling_show = x.show;
                }
                return false;
            });
            if (maybe_prev_sibling_show) {
                prev_sibling_show_cleanup_list =
                    maybe_prev_sibling_show.effects_off_list_for_section(null);
            }
        }

        var maybe_prev_show = this.playlist.get(section_index - 1);
        var prev_show_cleanup_list = [];
        if (maybe_prev_show instanceof Show) {
            prev_show_cleanup_list =
                maybe_prev_show.effects_off_list_for_section(null);
        }

        var cleanup_list = [];
        var use_effects = section ? section.use_effects : {};
        var prev_section = null;
        this.walk_backward(false, section, function (x) {
            if (x instanceof Show) {
                return true;
            } else {
                prev_section = x;
                return false;
            }
        });
        if (prev_section) {
            var prev_effects = prev_section.use_effects;
            var prev_show = prev_section.show;
            for (var k in prev_effects) {
                var e = this.effects[k];
                // a button effect with persist=true (default) is cleaned up
                // along with the section it's used in.  A button effect with
                // persist=false gets cleaned up like a require - as soon as
                // we get to a section where it is not used.
                if (e.persist) {
                    //XXX side effect
                    prev_section.cleanup_hook.push(method_caller(e, "cleanup"));
                } else if (! (k in use_effects)) {
                    cleanup_list.unshift(new EffectOffEvent(prev_show.effects[k]));
                }
            }
        }
        return prev_show_cleanup_list.concat(prev_sibling_show_cleanup_list, cleanup_list);
    },
    effects_on_list_for_section: function (section, start_from_here) {
        if (start_from_here) {
            var prev_section = null;
        } else {
            this.walk_backward(false, section, function (x) {
                if (x instanceof Show) {
                    return true;
                } else {
                    prev_section = x;
                    return false;
                }
            });
        }
        var list = [];
        if (section) {
            var use_effects = section.use_effects;
            var prev_effects = prev_section ? prev_section.use_effects : {};
            for (var k in use_effects) {
                if (! (k in prev_effects)) {
                    list.push(new EffectOnEvent(section.show.effects[k]));
                }
            }
        }
        return list;
    }
};


function TransitionEvent () {
}
TransitionEvent.prototype = {
    constructor: TransitionEvent,
    locktime: 1,
    toString: function () {
        return "#<TransitionEvent>";
    },
    call: function () {
        throw new Error("TransitionEvent abstract base class called");
    }
};


function SectionCleanupEvent (section) {
    this.section = section;
    this.locktime = section.lock_cleanup;
}
SectionCleanupEvent.prototype = {
    constructor: SectionCleanupEvent,
    __proto__: TransitionEvent.prototype,
    toString: function () {
        return "#<SectionCleanupEvent "+this.section.tag+">";
    },
    section: null,
    call: function () {
        var section = this.section;
        var ncleanup_hook = section.cleanup_hook.length;
        if (ncleanup_hook > 0) {
            for (var i = 0; i < ncleanup_hook; ++i) {
                section.cleanup_hook[i]();
            }
            section.cleanup_hook = [];
        }
        if (section.plugin) {
            section.plugin.cleanup();
        }
        var cleanup_script = section.cleanup_script;
        if (file_readable(cleanup_script)) {
            log("cleanup "+section.tag);
            script_play(cleanup_script, section.show);
            return true;
        } else if (this.locktime > 1) {
            print("warning: "+section.tag+" specified a lock_cleanup time "+
                  "but no cleanup script was found, so lock_cleanup will be ignored.");
            return false;
        }
    }
};

function SetAssetDirectoriesEvent (show) {
    this.show = show;
}
SetAssetDirectoriesEvent.prototype = {
    constructor: SetAssetDirectoriesEvent,
    __proto__: TransitionEvent.prototype,
    toString: function () {
        return "#<SetAssetDirectoriesEvent>";
    },
    show: null,
    call: function () {
        this.show.set_asset_directories()
        return false;
    }
};

function EffectOnEvent (effect) {
    this.effect = effect;
}
EffectOnEvent.prototype = {
    constructor: EffectOnEvent,
    __proto__: TransitionEvent.prototype,
    toString: function () {
        return "#<EffectOnEvent "+this.effect.name+">";
    },
    effect: null,
    call: function () {
        if (this.effect.require_p) {
            return this.effect.require_on();
        }
        return false;
    }
};

function EffectOffEvent (effect) {
    this.effect = effect;
}
EffectOffEvent.prototype = {
    constructor: EffectOffEvent,
    __proto__: TransitionEvent.prototype,
    toString: function () {
        return "#<EffectOffEvent "+this.effect.name+">";
    },
    effect: null,
    call: function () {
        return this.effect.require_off();
    }
};


/*
 * Utils: showstep
 */

function log (s) {
    print("[" + format_timestamp() + "] " + s);
    Ds.SetObjectAttrUsingRef(dsrefs.system.message, s);
    status_priority = 0;
}

function script_play (path, show, lock, callback) {
    if (show) {
        show.set_asset_directories();
    }
    last_called_script = path;
    print("["+format_timestamp()+"] script.play "+take_filename(path)+" ("+path+")");
    Ds.SetObjectAttrUsingRef(dsrefs.script.play, path);
    if (lock && callback) {
        sleep_with_lock(lock, callback, last_called_script);
    } else {
        Ds.Wait(0);
    }
}


/*
 * Effect
 */

function effect_register (module_name, module) {
    if (! (module_name in effect_modules)) {
        effect_modules[module_name] = module;
    }
}

function Effect (show) {
    this.show = show;
}
Effect.prototype = {
    constructor: Effect,
    show: null,
    persist: false //XXX: used by child classes effect_require and
                   //     ButtonFx, but not really germain to the parent
                   //     class.
};


/*
 * World
 *
 *  - local to the show
 */

function WorldNodeTransitionEvent (script, show) {
    this.script = script;
    this.show = show;
}
WorldNodeTransitionEvent.prototype = {
    constructor: WorldNodeTransitionEvent,
    __proto__: TransitionEvent.prototype,
    toString: function () {
        return "#<WorldNodeTransitionEvent "+
            take_basename(this.script) +">";
    },
    script: null,
    show: null,
    call: function () {
        if (file_readable(this.script)) {
            log("world-node-transition: "+this.script);
            script_play(this.script, this.show);
            return true;
        }
        return false;
    }
};

function WorldTransitionEvent (from, to, show) {
    this.from = from;
    this.to = to;
    this.show = show;
    this.from_off_script = this.from.get_off_script();
    this.to_on_script = this.to.get_on_script();
    this.to_node_script = this.to.get_node_script();
    //XXX hardcoded locktimes is super dodgy
    if (file_readable(this.from_off_script)) {
        this.locktime = 5;
    } else {
        this.locktime = 4;
    }
}
WorldTransitionEvent.prototype = {
    constructor: WorldTransitionEvent,
    __proto__: TransitionEvent.prototype,
    toString: function () {
        return "#<WorldTransitionEvent " +
            this.from.name + " to " + this.to.name +">";
    },
    from: null,
    to: null,
    show: null,
    call: function () {
        var script = [];
        if (file_readable(this.from_off_script)) {
            script.push("script include "+this.from_off_script);
            script.push("+1");
        }
        script.push("script include "+this.to_on_script);
        script.push("+3");

        script.push("script include "+this.to_node_script);

        Ds.SendScriptCommands("showstep-world-transition",
                              format_script(script));
        return true;
    }
};

function World (show, name) {
    Effect.call(this, show);
    this.name = name;

    //XXX warn if world doesn't exist on disk

    //XXX maybe load world graph?

}
World.prototype = {
    constructor: World,
    __proto__: Effect.prototype,
    name: null,
    node: null,
    get_path: function () {
        return join_paths(this.show.worldsroot, this.name);
    },
    get_on_script: function () {
        return join_paths(this.get_path(), this.name + "-on.ds");
    },
    get_off_script: function () {
        return join_paths(this.get_path(), this.name + "-off.ds");
    },
    get_node_script: function () {
        return join_paths(this.get_path(), "graph/"+this.node+".ds");
    },
    get_node_transition_script: function (other) {
        return join_paths(this.get_path(), "graph/"+this.node+"--"+other.node+".ds");
    },
    get_node_transition_effect: function (other) {
        if (other.name == this.name) {
            var node_trans_script = this.get_node_transition_script(other);
            return new WorldNodeTransitionEvent(node_trans_script, this.show);
        } else {
            return new WorldTransitionEvent(this, other, this.show);
        }
    }
};


/*
 * Button Fx
 */

var button_fx_radio_groups = {};

function ButtonFx (show, name, options) {
    Effect.call(this, show);
    options = merge_defaults(options, {});
    if ("name" in options) {
        this.name = options.name;
    } else {
        this.name = name + fx_counter;
        fx_counter++;
    }
    if ("persist" in options) {
        this.persist = options.persist;
        if (typeof this.persist == "string") {
            this.persist = JSON.parse(this.persist);
        }
    }
    if ("group" in options) {
        this.group = options.group;
    }
    if ("spec" in options) {
        this.spec = options.spec;
    }
    //XXX a bit weird that this sets up its own callback
    //
    //XXX we're going to have to work out the plumbing of how a callback
    //    in a nested show is found.
    //
    show.fx_callbacks[this.name] = this;
    if (this.group && ! (this.group in button_fx_radio_groups)) {
        button_fx_radio_groups[this.group] = [];
    }
}
ButtonFx.prototype = {
    constructor: ButtonFx,
    __proto__: Effect.prototype,
    name: null,
    button: null,
    label: null,
    group: null,
    spec: null,
    require_p: false,
    button_refs: null,
    last_call: 0,

    // Internal
    //
    doubleclick_protection: function () {
        var now = Ds.GetObjectAttrUsingRef(dsrefs.system.time);
        if (now - this.last_call < 1) {
            return false;
        } else {
            this.last_call = now;
            return true;
        }
    },

    // Button interface
    //
    //   Child effects should call these methods
    //
    setup: function (button, label, button_refs, props) {
        this.button = button;
        this.label = label;
        this.button_refs = button_refs;
        props.label = "\""+label+"\"";
        props.bordercolor = "65 65 65";
        props.command = "\"showstep call "+this.name+"\"";
        if (this.group) {
            button_fx_radio_groups[this.group].push(this);
        }
    },
    cleanup: function () {
        this.remove_from_group();
    },
    call: function (radio_on_p) {
        // radio_on_p means we are going into an 'on' state, so others in
        // group should be turned off.
        if (radio_on_p && this.group) {
            this.radio_others_off();
        }
    },

    // Require interface
    //
    require_on: function () {
    },
    require_off: function () {
        this.cleanup();
    },

    // Radio interface
    //
    radio_get_status: function () {
        return false;
    },
    radio_self_off: function () {
        // does nothing
    },
    radio_others_off: function () {
        if (this.group) {
            var gr = button_fx_radio_groups[this.group];
            for (var i = 0, n = gr.length; i < n; ++i) {
                var o = gr[i];
                if (o != this) {
                    if (o.radio_get_status()) {
                        if (o.button_refs) {
                            Ds.SetObjectAttrUsingRef(o.button_refs.bordercolor,
                                                     { r: 65, g: 65, b: 65 });
                        }
                        o.radio_self_off();
                    }
                }
            }
        }
    },
    remove_from_group: function () {
        if (this.group) {
            var gr = button_fx_radio_groups[this.group];
            var i = gr.indexOf(this);
            gr.splice(i, 1);
        }
    }
};
var button_fx = ButtonFx; // backward compatibility with show fx modules


/**
 * fx is responsible for loading fx modules if they need to be loaded, and
 * instantiating the effect for the button.
 */
//XXX pretty sure we no longer have any shows that use the old fx form of
//    button, so it should be safe to rename this to something more
//    meaningful, or refactor its functionality into another procedure.
function fx (show, module_name) {
    var args = [];
    //XXX: Array.prototype.slice.call(arguments) fails in D6 6.18.08.3
    for (var i = 0, n = arguments.length; i < n; ++i) {
        args.push(arguments[i]);
    }
    // is module already loaded?
    if (! effect_modules[module_name]) {
        // load it
        if (! show) {
            throw new Error("Cannot create ButtonFx (not in a show)");
        }
        var search_path = [show.fx_directory, fx_load_path];
        for (var i = 0, n = search_path.length; i < n; ++i) {
            var module_path = join_paths(search_path[i], module_name + ".js");
            if (file_readable(module_path)) {
                include(Ds.ResolvePathName(module_path));
                break;
            }
        }
    }
    var module = effect_modules[module_name];
    if (! module || ! module.prototype ||
        ! (module.prototype instanceof Effect)) //XXX: would be good to have more specific check for the context
    {
        delete effect_modules[module_name];
        throw new Error("module "+module_name+" failed to register");
    }
    return construct(module, args); // args = same arguments as to this fx call
}

function effect_spec_parse (show, spec, extra_options) {
    if (spec[0] == "@") {
        var ref = spec.substr(1);
        var effect = show.effects[ref];
        if (! effect) {
            throw new Error("invalid effect reference: "+spec);
        }
        return effect;
    }
    var sp = spec.split(":");
    var module_name = sp[0];
    var args = [show, module_name];
    var options = {};
    for (var i = 1, n = sp.length; i < n; ++i) {
        var argspec = sp[i];
        var equalsign = argspec.indexOf("=");
        if (equalsign > -1) {
            // it's a keyword
            var key = argspec.substr(0, equalsign);
            var val = argspec.substr(equalsign + 1);
            options[key] = val;
        } else {
            args.push(argspec);
        }
    }
    if (extra_options) {
        merge_objects(options, extra_options);
    }
    args.push(options);
    return fx.apply(this, args); // args = [show, module_name, *args, options]
}

function button_spec_parse (show, spec) {
    if (typeof spec == "object") {
        if ("action" in spec) {
            var action_spec = spec.action;
        } else {
            throw new Error("Button spec is missing 'action'");
        }
        if ("label" in spec) {
            var label = spec.label;
            var spec_nparts = 2;
        } else {
            spec_nparts = 1;
        }
    } else { // string
        var sp = spec.split("::");
        spec_nparts = sp.length;
        if (spec_nparts > 2) {
            throw new Error("too many components in \""+spec+"\"");
        } else if (spec_nparts == 2) {
            var label = sp[0];
            var action_spec = sp[1];
        } else if (spec_nparts == 1) {
            // label will be action.name (see below)
            action_spec = sp[0];
        } else {
            throw new Error("Empty button spec");
        }
    }
    var action = effect_spec_parse(show, action_spec, { spec: spec });
    if (! (action instanceof ButtonFx)) {
        throw new Error("invalid button action in \""+spec+"\"");
    }
    if (spec_nparts == 1) {
        label = action.name;
    }
    if (action_spec[0] != "@" && ! action.hasOwnProperty("persist")) {
        action.persist = true;
    }
    //XXX a bit weird for this procedure to be installing the action when
    //    it's called parse
    show.effects[action.name] = action;
    return [label, action];
}


/*
 * Non-blocking sleep
 */

function sleep_clear () {
    Ds.SetTimerEvent(1e12, 'system'); //XXX: hack for D5 16.04
    timers = [];
}

function sleep_callback () {
    var actions = [];
    var now = Ds.GetObjectAttrUsingRef(dsrefs.system.time);
    var i, n;
    for (i = 0, n = timers.length; i < n; ++i) {
        var timer = timers[i];
        if (timer.endtime > now) {
            break;
        }
        actions.push(timer.action);
    }
    timers = timers.slice(i);
    if (i < n) {
        timers[0].duration = timers[0].endtime - now;
        Ds.SetTimerEvent(timers[0].duration, 'system');
    } else {
        Ds.SetTimerEvent(1e12, 'system'); //XXX: hack for D5 16.04
    }
    // actions might modify the list of timers, so we call them last.
    for (var j = 0; j < i; ++j) {
        actions[j]();
    }
}

function sleep (sec, method, owner) {
    var now = Ds.GetObjectAttrUsingRef(dsrefs.system.time);
    var timer = {
        starttime: now,
        endtime: now + sec,
        duration: sec,
        action: method,
        owner: owner
    };
    var i = timers.findIndex(
        function (x) { return x.endtime > timer.endtime; });
    if (i == -1) {
        timers.push(timer);
    } else {
        timers.splice(i, 0, timer);
    }
    var first = timers[0];
    var sleep_for = first.endtime - now;
    if (sleep_for > 0) {
        Ds.SetTimerEvent(sleep_for, "system");
    } else {
        sleep_callback();
    }
}

function lock_countdown () {
    var now = Ds.GetObjectAttrUsingRef(dsrefs.system.time);
    var sec = lock_until - now;
    var section = the_show.current_section(true) || { tag: "[no section]" };
    log(section.tag+" lock "+format_lock_duration(sec));
    if (sec > 10) {
        sleep(10, lock_countdown);
    }
}

function sleep_extend_with_lock (owner, sec) {
    var i = timers.findIndex(
        function (x) { return x.owner == owner; });
    if (i < 0) {
        return false;
    }
    var old = timers[i];
    timers.splice(i, 1);
    var now = Ds.GetObjectAttrUsingRef(dsrefs.system.time);
    var end_time = now + sec;
    var new_action = function () {
        cp.update_lock_state();
        old.action();
    };
    lock_until = end_time;
    Ds.SetObjectAttrUsingRef(cp.refs.nextbuttoncolor,
                             { r: 100, g: 0, b: 0 });
    sleep(sec, new_action, owner);
    lock_countdown();
    return true;
}

function sleep_with_lock (sec, method, owner) {
    var call_method_and_unlock = function () {
        cp.update_lock_state();
        method();
    };
    var now = Ds.GetObjectAttrUsingRef(dsrefs.system.time);
    var end_time = now + sec;
    lock_until = end_time;
    Ds.SetObjectAttrUsingRef(cp.refs.nextbuttoncolor,
                             { r: 100, g: 0, b: 0 });
    sleep(sec, call_method_and_unlock, owner);
    lock_countdown();
}

function lock (sec) {
    var now = Ds.GetObjectAttrUsingRef(dsrefs.system.time);
    var end_time = now + sec;
    if (end_time > lock_until) {
        lock_until = end_time;
        Ds.SetObjectAttrUsingRef(cp.refs.nextbuttoncolor,
                                 { r: 100, g: 0, b: 0 });
        sleep(sec, method_caller(cp, "update_lock_state"));
        lock_countdown();
    }
}

function lock_clear () {
    lock_until = 0;
}

function locked () {
    var now = Ds.GetObjectAttrUsingRef(dsrefs.system.time);
    return (now < lock_until);
}


/**
 * refresh_button_fx is used in show development, and called only from the
 * outside.  Its job is to reload effect modules for buttons, and reparse
 * all menus from their sources, and update the UI.
 */
function refresh_button_fx () {
    log("refresh_button_fx needs to be rewritten.  doing nothing.");
    return;
    var section = the_show.current_section();
    for (var k in effect_modules) {
        var v = effect_modules[k];
        if (v instanceof ButtonFx) {
            delete effect_modules[k];
        }
    }
    section.menu = [null, null, null, null, null,
                    null, null, null, null, null,
                    null, null, null];
    showstep_button_callback();
}

function section_play (section, start_from_here, showstep5_details) {
    // section may be null if we are advancing off the end of the playlist

    lock(1); // doubleclick guard

    if (section) {
        var show = section.show;
    } else {
        show = the_show;
    }

    // Notes and Control panel
    //
    if (section) {
        var title = format_show_title(take_path_component(show.showroot));
    } else {
        title = "";
    }
    Ds.SetObjectAttrUsingRef(cp.refs.title, title);
    Ds.SetObjectAttrUsingRef(cp.refs.currentsection,
                             format_section(section));
    cp.format_notes(section, "current");
    if (section) {
        var section_index = show.playlist.list.indexOf(section);
    } else {
        section_index = show.playlist.length();
    }
    cp.update_next(show, section);
    cp.buttons_init(section);

    // check for autoplay when section lock expires
    //
    var autoplay_run = function () {
        var next_section = the_show.next_section(true);
        var next_section_index = next_section && next_section.show.playlist.list.indexOf(next_section);
        if ((section && section.autoadvance) ||
            (next_section && (next_section.autoplay ||
                              (next_section_index == 0 && next_section.show.autoplay))))
        {
            section_play(next_section, false);
        }
    };

    // cleanup completed topics, then call new section.  each cleanup
    // script gets 1 second of auto-lock, and may lock for more.
    //
    if (start_from_here) {
        var cleanup_list = [];
    } else {
        cleanup_list = show.cleanup_list_for_section(section);
    }
    // handle the use_effects list as part of the cleanup sequence
    var use_effects = section ? section.use_effects : {};

    // stale effects to turn off?
    if (start_from_here) {
        var effects_off = [];
    } else {
        //XXX this call has a nasty side effect on prev_section
        effects_off = show.effects_off_list_for_section(section);
    }

    // world node transition
    var world = section && section.world;
    var world_change = [];
    if (world) {
        if (show.current_world_node && world.node) {
            if (show.current_world_node.name == world.name) {
                // node transitions within the same world
                if (show.current_world_node.node != world.node) {
                    var r = show.current_world_node.get_node_transition_effect(world);
                    if (r) {
                        world_change.push(r);
                    }
                }
            } else {
                // world change - prev world off, new world on
                var r = show.current_world_node.get_node_transition_effect(world);
                if (r) {
                    world_change.push(r);
                }
            }
        }
    }

    // new effects to turn on?
    var effects_on = show.effects_on_list_for_section(section, start_from_here);

    var transition_events = effects_off.concat(
        cleanup_list, world_change,
        [new SetAssetDirectoriesEvent(show)],
        effects_on);

    // advance indexes
    //
    show.playlist.index = section_index;
    var pshow = show;
    while (pshow.parent) {
        pshow.parent.playlist.index =
            pshow.parent.playlist.list.indexOf(pshow);
        pshow = pshow.parent;
    }

    var transition_run = function () {
        // handle effects_off, cleanup_list, world_change, effects_on
        for (var i = 0, n = transition_events.length; i < n; ++i) {
            var event = transition_events[i];
            if (event.call()) {
                transition_events.splice(0, i + 1);
                sleep_with_lock(event.locktime, transition_run, last_called_script);
                return;
            }
        }

        // ui update
        //
        cp.playlist_update();
        cp.update_lock_state();

        // call new section
        //
        if (section) {
            log("section "+section.tag);
            section.show.set_asset_directories();
            Ds.SetObjectAttrUsingRef(dsrefs.showstep.acceptinsert, section.accept_insert);

            // update effect cursor with current section, if we can start
            // from it.  We aren't able to restart a section inside of a
            // spliced playlist, so we only do this if the section is in
            // the main playlist.
            var in_original_playlist_p = section.show.spec.playlist.find(function (x) {
                return x.section == section.tag;
            });
            var main_playlist_section = section.show.get_section_by_tag(section.tag);
            var setup_script = main_playlist_section.setup_script;
            var have_setup_script = setup_script && file_readable(setup_script);
            if (in_original_playlist_p && (have_setup_script || main_playlist_section.world)) {
                Ds.SetObjectAttr("showstep5", "command",
                                 "find_provider('Showstep3Shim')()"+
                                 ".message_set_options('section="+section.tag+"')");
            }

            // Show the menu for this section in the UI.
            //
            //   Items in show.runtime_menus[section.tag] override
            // items in show.menus[section.tag].
            //
            ui_show_section_menu(section);
            if (start_from_here && section.setup_override_play) {
                // just run autoplay
                sleep_with_lock(1, autoplay_run, last_called_script);
            } else {
                last_called_script = section.play_script;
                sleep_with_lock(section.lock, autoplay_run, last_called_script);
                if (section.plugin) {
                    section.plugin.play();
                } else if (file_readable(section.play_script)) {
                    print("["+format_timestamp()+"] script.play "+
                          take_filename(section.play_script)+
                          " ("+section.play_script+")");
                    Ds.SetObjectAttrUsingRef(dsrefs.script.play, section.play_script);
                }
            }
        } else {
            Ds.Wait(0);
            router.quit();
            Ds.Wait(0);
            if (showstep5_details) {
                if (showstep5_details.onquit_restart_p) {
                    Ds.Wait(0.1);
                    Ds.SetObjectAttr("showstep5", "operationLock", 0);
                    Ds.SetObjectAttr("showstep5", "command",
                                     "restart('"+showstep5_details.effect_object+"')");
                } else {
                    var parents = Ds.GetObjectParentNames(showstep5_details.effect_object);
                    var nparents = parents.length;
                    if (nparents > 0 && parents[0] != "showstep5") {
                        Ds.SetObjectAttr(parents[0], "override", false);
                    }
                    // turn off any child effects
                    var nchildren = Ds.GetObjectNumChildren(showstep5_details.effect_object);
                    if (nchildren) {
                        Ds.SetObjectAttr("showstep5", "command",
                                         "off('"+showstep5_details.effect_object+"')");
                    } else { // shortcut
                        Ds.DeleteObject(showstep5_details.effect_object);
                    }
                }
            }
        }
    };

    transition_run();
}

function section_reset_and_play (ashow, index, suppress_reset) {
    if (index == null) {
        index = ashow.start_index;
    }
    var section = ashow.playlist.get(index);
    var world = section.world;
    var setup_script = section.setup_script;
    var have_setup_script = setup_script && file_readable(setup_script);
    if (index > 0 && setup_script && ! have_setup_script && ! world) {
        // take no action, and do not clobber the_playlist if we cannot proceed.
        log("No setup script or world found for section "+section.tag+". Cannot continue.");
        return;
    }

    // Stop all running scripts and clear lock and pending actions.
    //
    Ds.ExecuteObjectCommand("script", "stopall");
    Ds.ExecuteObjectCommand("js", "stopall");
    sleep_clear();
    lock_clear();
    Ds.Wait(0);

    // Fade and reset
    //
    the_show = ashow;
    the_show.playlist.index = index;
    the_show.current_world_node = null;
    if (! suppress_reset) {
        active_cameras_off(3);
        Ds.Wait(3, "system");
        Ds.ExecuteObjectCommand("system", "reset");
    }
    the_show.populate_fx_callbacks();
    Ds.Wait(0);

    // Call: world, setup, section
    //
    var step4_call_section = function () {
        section_play(section, true);
    };

    var step3_call_setup_script = function () {
        if (have_setup_script) {
            var setup_script = section.setup_script;
            script_play(setup_script, section.show, section.lock_setup, step4_call_section);
        } else { // first section of show
            step4_call_section();
        }
    };

    var step2_call_world_node = function () {
        if (world && world.node) {
            var node_script = world.get_node_script();
            script_play(node_script, null, 1, step3_call_setup_script);
        } else {
            step3_call_setup_script();
        }
    };

    if (world) {
        script_play(world.get_on_script(), world.show, 3, step2_call_world_node);
    } else {
        step2_call_world_node();
    }
}


function showstep_try_play_path (path, suppress_reset) {
    try {
        var ashow = new Show(path);
    } catch (e) {
        log("Error: not a showstep playable: "+path);
        print(e);
        return;
    }
    section_reset_and_play(ashow, null, suppress_reset);
}


/*
 * Callback API: Util
 */

function ui_show_section_menu (section) {
    // when called, we can assume the whole menu ui has just been reset to
    // defaults.
    var show = section.show;
    var tag = section.tag;
    var runtime_menu = show.runtime_menus[tag] || {};
    var menu = show.menus[tag] || {};
    var button_props = {};
    for (var b = 1; b <= 12; ++b) {
        var menuitem = runtime_menu[b] || menu[b] || null;
        if (menuitem) {
            var label = menuitem[0];
            var ref = menuitem[1];
            var action = show.effects[ref];
            var button_refs = cp.refs.buttons[b];
            var button_dsob = "showstep_button"+b;
            button_props[button_dsob] = {};
            try {
                action.setup(b, label, button_refs, button_props[button_dsob]);
            } catch (e) {
                print("Menu item setup failed for button '"+label+"': "+e);
            }
        }
    }
    props_struct_to_script_and_call(button_props);
}

function showstep_button_callback () {
    var section = the_show.current_section(true);
    if (! section) {
        return;
    }
    var show = section.show;
    if (! (section.tag in show.runtime_menus)) {
        show.runtime_menus[section.tag] = {};
    }
    var menu = show.runtime_menus[section.tag];
    var button_props = {};
    for (var b = 1; b <= 12; ++b) {
        var spec = Ds.GetObjectArrayElemUsingRef(dsrefs.showstep.button, b);
        if (spec) {
            var existing_action = (b in menu) && show.effects[(menu[b][1])];
            if ((! existing_action) || existing_action.spec != spec) {
                var button_dsob = "showstep_button"+b;
                try {
                    var parsed = button_spec_parse(show, spec);
                    var label = parsed[0];
                    var action = parsed[1];
                    menu[b] = [label, action.name];
                    show.effects[action.name] = action;
                    var button_refs = cp.refs.buttons[b];
                    button_props[button_dsob] = {};
                    action.setup(b, label, button_refs, button_props[button_dsob]);
                    section.use_effects[action.name] = true;
                } catch (e) {
                    button_props[button_dsob] = {
                        label: "ERROR",
                        bordercolor: "100 0 0",
                    };
                    print("error setting up button "+b+": "+e);
                }
            }
        }
    }
    props_struct_to_script_and_call(button_props);
}

//XXX: the control panel playlist buttons could use this 'call' mechanism
function showstep_call_callback () {
    var callback_id = Ds.GetObjectAttrUsingRef(dsrefs.showstep.call);
    if (callback_id == "") {
        return;
    }
    try {
        var show = the_show.current_show();
        var callback = show.fx_callbacks[callback_id];
        if (callback) {
            if (callback.doubleclick_protection()) {
                callback.call();
            }
        } else {
            print("error: invalid callback id "+callback_id);
        }
    } catch (e) {
        print("error: error in callback for "+callback_id+": "+e);
    }
}

function showstep_debug_callback () {
    var expression = Ds.GetObjectAttrUsingRef(dsrefs.showstep.debug);
    try {
        if (expression != "") {
            print("debug:: "+expression);
            print(eval(expression));
        }
    } catch (e) {
        print("error: "+e);
    }
}

function showstep_status_callback () {
    var s = Ds.GetObjectAttrUsingRef(dsrefs.showstep.status);
    print("DEPRECATION: showstep status ("+s+")");
    Ds.SetObjectAttrUsingRef(dsrefs.system.message, s);
    status_priority = 1;
}

function showstep_lock_callback () {
    var d = Ds.GetObjectAttrUsingRef(dsrefs.showstep.lock);
    if (d <= 0) {
        return;
    }
    // does last_called_script have a timer running?
    //
    if (sleep_extend_with_lock(last_called_script, d)) {
        var owner = "";
        if (last_called_script) {
            owner = take_filename(last_called_script) + " ";
        }
        print("[" + format_timestamp() + "] " + owner + "lock " + d + " seconds");
    } else {
        // section lock
        //
        lock(d);
    }
}

function showstep_world_callback (dsob, attr) {
    // For backwards compatibility, attr may be either "scene" (old) or
    // "world" (new).
    var show = the_show.current_show();
    var spec = Ds.GetObjectAttrUsingRef(dsrefs.showstep[attr]);
    if (! spec) {
        show.current_world_node = null;
        return;
    }
    show.current_world_node = show.world_node_get_create(spec);
    print("[" + format_timestamp() + "] world set to " +
          show.current_world_node.name + ":" + show.current_world_node.node);
}

function showstep_message_refresh_ui_callback () {
    var section = the_show.current_section(true);
    var next_section = the_show.next_section(true, section);
    var show = section.show;

    // refresh notes from .show file
    var show_notes = file_read_json(
        join_paths(show.script_directory, "show-notes.json"),
        {});
    if (show.source.slice(-5) == ".show") {
        // there is a .show file to get notes from
        var show_def = file_read_json(show.source, null);
        if (show_def) {
            var def_playlist = show_def.playlist;
            show_notes = merge_defaults(show_def.notes, show_notes);
            for (var i = 0, n = def_playlist.length; i < n; ++i) {
                var section_def = def_playlist[i];
                if ("notes" in section_def) {
                    show_notes[section_def.section] = section_def.notes;
                }
            }
        }
    }
    show.notes = show_notes;

    cp.update_lock_state();
    Ds.SetObjectAttrUsingRef(cp.refs.currentsection,
                             format_section(section));
    cp.format_notes(section, "current");
    if (next_section) {
        cp.format_notes(next_section, "next");
    }
    cp.update_next(show, section);
    ui_show_section_menu(section);
    cp.playlist_update();
}


/*
 * Callback API: Play
 */

function showstep_insert (path) {
    log("insert "+path);

    try {
        var ashow = new Show(path);
    } catch (e) {
        log("Error: not a showstep section: "+path);
        print(e);
        return;
    }

    // if we are not in a section, we need to suppress cleanup
    var in_a_section_p = the_show.current_section(true) && true;

    // Insert new items into playlist after any upcoming autoplays,
    // disallowing duplicates
    //
    var insert_into_show = the_show.current_show();
    while (insert_into_show && insert_into_show.autoplay &&
           insert_into_show.parent)
    {
        insert_into_show = insert_into_show.parent;
    }

    //XXX: where are the inserted section's script.directory & friends?
    //     standalone sections will probably want them to be subdirectories.
    var playlist_len = insert_into_show.playlist.length();
    var insertion_point = insert_into_show.playlist.index;
    var whatsthere = insert_into_show.playlist.get(insertion_point);
    var cur_section = whatsthere;
    var ip = insertion_point;
    while (cur_section instanceof Show) {
        ip--;
        cur_section = insert_into_show.playlist.get(ip);
    }
    if (cur_section instanceof Section && ! cur_section.accept_insert) {
        log("Show does allow vignette insertion here.");
        return;
    }
    if (whatsthere instanceof Show && whatsthere.source == ashow.source) {
        log("Duplicate insertion not allowed.");
        return;
    }
    if (insertion_point < playlist_len) {
        insertion_point++;
    }
    // move insertion point past autoplays and check for duplicates
    for (; insertion_point < playlist_len; ++insertion_point) {
        var whatsthere = insert_into_show.playlist.get(insertion_point);
        if (whatsthere.autoplay) {
            if (whatsthere instanceof Show && whatsthere.source == ashow.source) {
                log("Duplicate insertion not allowed.");
                return;
            }
        } else {
            break;
        }
    }
    whatsthere = insert_into_show.playlist.get(insertion_point);
    if (whatsthere instanceof Show && whatsthere.source == ashow.source) {
        log("Duplicate insertion not allowed.");
        return;
    }

    // put the new show into the playlist at the insertion point
    insert_into_show.playlist.insert(insertion_point, ashow);

    cp.playlist_update();
    cp.update_lock_state();

    if (the_show.next_section(true) == ashow.playlist.get(0)) {
        cp.update_next(the_show);

        // If the inserted section is next, and it's an autoplay, and we
        // are not locked, play it now.
        //
        if (! locked() && ashow.autoplay) {
            var section = ashow.playlist.get(ashow.start_index);
            section_play(section, ! in_a_section_p);
        }
    }
}

function showstep_insert_callback () {
    var path = Ds.GetObjectAttrUsingRef(dsrefs.showstep.insert);
    if (path == "") {
        return;
    }
    showstep_insert(path);
}

function showstep_message_insert_callback (message_cmd, path) {
    if (path == "") {
        return;
    }
    showstep_insert(path);
}

function showstep_splice (playlist_name, replace) {
    log("splice "+playlist_name);

    replace = replace || 0;

    // Is 'path' the name of an alternative playlist in the_show?
    // If so, splice it in...
    //
    //    This is kind of horrible to be modifying the playlist
    //    with a splice that leaves no record of what was originally
    //    spliced.  But, is there any case in which we would want
    //    alternative playlists to be treated as a Show rather than
    //    as just a sublist of sections?
    //

    // if we are not in a section, we need to suppress cleanup
    var in_a_section_p = the_show.current_section(true) && true;

    // The idea is:
    //
    // You can splice an alternative playlist from the current show, or
    // any ancestor of the current show, into the show that contains that playlist.
    //

    var current_show = the_show.current_show();
    var playlist_show = current_show;
    do {
        if (playlist_name in playlist_show.def_playlists) {
            var pl = playlist_show.def_playlists[playlist_name];
        } else {
            playlist_show = playlist_show.parent;
        }
    } while (! pl && playlist_show);
    if (! pl) {
        throw new Error("playlist not found: " + playlist_name);
    }

    // Now we have playlist (pl) and the show it belongs in.
    // Insert it after the current index.

    pl = playlist_show._construct_a_playlist(
        pl, playlist_show.def_defaults, playlist_show.def_menus)

    // After autoplays?

    // Disallow duplicates?

    var insertion_point = playlist_show.playlist.index;
    if (insertion_point < playlist_show.playlist.length()) {
        insertion_point++;
    }

    // Splice it in
    var args = [insertion_point, replace];
    args = args.concat(pl.list);
    Array.prototype.splice.apply(playlist_show.playlist.list, args);

    cp.playlist_update();
    cp.update_lock_state();

    var pl_first = pl.get(0);
    if (the_show.next_section(true) == pl_first) {
        cp.update_next(the_show);

        // If the inserted section is next, and it's an autoplay, and we
        // are not locked, play it now.
        //
        if (! locked() && pl_first.autoplay) {
            section_play(pl_first, ! in_a_section_p);
        }
    }
}


/**
 * Play is used only at the beginning of a show to play the first section.
 */
function showstep_play (path, suppress_reset) {
    log("play "+path);
    // is path a tag in the current playlist?

    //XXX: what we should really do here is create a new playable with its
    //     index set to the appropriate section, and all mutable state reset.
    //
    var section_index = the_show.playlist.find_index(function (x) { return x.tag == path; });
    if (section_index > -1) {
        section_reset_and_play(the_show, section_index);
    } else { // look for playlist or standalone section
        showstep_try_play_path(path, suppress_reset);
    }
}

function showstep_play_callback () {
    var path = Ds.GetObjectAttrUsingRef(dsrefs.showstep.play);
    if (path == "") {
        return;
    }
    showstep_play(path);
}

function showstep_message_play_callback (message_cmd, path) {
    if (path == "") {
        return;
    }
    showstep_play(path);
}

function showstep_next_callback (message_cmd, details) {
    if (details) { // showstep5 effect cursor information
        var sp = details.split(" ");
        var showstep5_details = {
            effect_object: sp[0],
            onquit_restart_p: (sp[1] == "onquit=restart")
        };
    } else {
        showstep5_details = null;
    }
    var nextsection = the_show.next_section(true);
    if (! nextsection && ! the_show.current_section(true)) {
        log("Nothing to do");
    } else if (locked()) {
        if (nextsection) {
            var s = "Cannot play "+nextsection.tag;
        } else {
            s = "Cannot cleanup";
        }
        log(s+"; wait until current section finishes.");
    } else {
        section_play(nextsection, false, showstep5_details);
    }
}


/*
 * Startup commands
 */

function Command (name) {
    this.name = name;
}
Command.prototype = {
    constructor: Command,
    name: null,
    call: function () {}
};

function CommandInsert (path) {
    Command.call(this, "insert");
    this.path = path;
}
CommandInsert.prototype = {
    constructor: CommandInsert,
    __proto__: Command.prototype,
    path: null,
    call: function () {
        showstep_insert(this.path);
    }
};

function insert (path) {
    return new CommandInsert(path);
}

function CommandPlay (path, suppress_reset) {
    Command.call(this, "play");
    this.path = path;
    this.suppress_reset = suppress_reset;
}
CommandPlay.prototype = {
    constructor: CommandPlay,
    __proto__: Command.prototype,
    path: null,
    suppress_reset: false,
    call: function () {
        showstep_play(this.path, this.suppress_reset);
    }
};

function play (path, suppress_reset) {
    return new CommandPlay(path, suppress_reset||false);
}


/*
 * Main
 */

function showstep_ensure_init () {
    if (object_exists("showstep")) {
        var init_script = join_paths(get_self_path(), "showstep-init.ds");
    } else {
        init_script = join_paths(get_self_path(), "showstep-init-full.ds");
    }
    Ds.SetObjectAttr("script", "play", init_script);
    while (! object_exists("showstep")) {
        Ds.Wait(0.1);
    }
    Ds.Wait(0.1); //XXX seems we need a little extra
}

function EventRouter () {
    this.attribute_handlers = {};
    this.command_handlers = {};
    this.message_handlers = {};
}
EventRouter.prototype = {
    constructor: EventRouter,
    toString: function () { return "#<EventRouter>"; },
    attribute_handlers: null,
    command_handlers: null,
    message_handlers: null,
    timer_handler: null,
    handle_messages: false,
    handle_timer: false,
    running: false,
    register_attribute: function (dsob, attr, handler) {
        this.attribute_handlers[dsob+"."+attr] = handler;
        Ds.AddObjectAttrEvent(dsob, attr);
        return this;
    },
    register_command: function (dsob, command, handler) {
        this.command_handlers[dsob+"."+command] = handler;
        Ds.AddObjectCommandEvent(dsob, command);
        return this;
    },
    register_message: function (message, handler) {
        this.message_handlers[message] = handler;
        if (! this.handle_messages) {
            this.handle_messages = true;
            Ds.SetMessageEvent();
        }
        return this;
    },
    register_timer_handler: function (handler) {
        this.timer_handler = handler;
        if (! this.handle_timer) {
            this.handle_timer = true;
            Ds.SetTimerEvent(1e12, "system");
        }
        return this;
    },
    message_parse: function (event) {
        var re = /([\S]+)\s*(.*)/;
        var m = re.exec(event);
        if (m) {
            return [m[1], m[2]];
        } else {
            return null;
        }
    },
    handle_event: function (event) {
        switch (typeof event) {
        case "DsObjectAttrRef":
            var dsob = Ds.GetAttrRefObjectName(event);
            var attr = Ds.GetAttrRefAttrName(event);
            var handler = this.attribute_handlers[dsob+"."+attr];
            handler(dsob, attr);
            break;
        case "DsObjectCommandRef":
            var dsob = Ds.GetCommandRefObjectName(event);
            var command = Ds.GetCommandRefCommandName(event);
            var handler = this.command_handlers[dsob+"."+command];
            handler(dsob, command);
            break;
        case "number": // timer interval
            if (this.timer_handler) {
                this.timer_handler(event);
            }
            break;
        case "string": // control message
            var sp = this.message_parse(event);
            if (sp) {
                var cmd = sp[0];
                var handler = this.message_handlers[cmd];
                if (handler) {
                    handler.apply(this, sp);
                }
            } else {
                print("Warning: malformed message: \""+event+"\"");
            }
            break;
        }
    },
    run: function () {
        this.running = true;
        while (this.running) {
            var event = Ds.WaitForEvent();
            if (typeof event == "object") {
                for (var i = 0, n = event.length; i < n; ++i) {
                    this.handle_event(event[i]);
                }
            } else {
                this.handle_event(event);
            }
        }
    },
    quit: function () {
        this.running = false;
    }
};

function start (path) {
    print("ShowStep version "+showstep_version+" starting");
    self_path = get_self_path();
    fx_load_path = join_paths(self_path, "fx");
    showstep_ensure_init();
    dsrefs_init();
    script_directories_reset();
    cp = new ControlPanelUI();
    router = new EventRouter()
        .register_attribute(showstep_dsob, "button", showstep_button_callback)
        .register_attribute(showstep_dsob, "call", showstep_call_callback)
        .register_attribute(showstep_dsob, "debug", showstep_debug_callback)
        .register_attribute(showstep_dsob, "insert", showstep_insert_callback)
        .register_attribute(showstep_dsob, "lock", showstep_lock_callback)
        .register_attribute(showstep_dsob, "play", showstep_play_callback)
        .register_attribute(showstep_dsob, "world", showstep_world_callback)
        .register_attribute(showstep_dsob, "scene", showstep_world_callback)
        .register_attribute(showstep_dsob, "status", showstep_status_callback)
        .register_command(showstep_dsob, "next", showstep_next_callback)
        .register_message("insert", showstep_message_insert_callback)
        .register_message("next", showstep_next_callback)
        .register_message("play", showstep_message_play_callback)
        .register_message("refresh_ui", showstep_message_refresh_ui_callback)
        .register_message("file_explorer_on_show", file_explorer_on_show)
        .register_message("fadestopreset", function () { router.quit(); })
        .register_message("quit", function () { router.quit(); })
        .register_timer_handler(sleep_callback);

    the_show = new Show();
    cp.refresh();

    if (path instanceof Command) {
        path.call();
    } else if (path) {
        showstep_try_play_path(path);
    }

    try {
        router.run();
    } catch (e) {
        print(e);
    } finally {
        the_show = new Show();
        Ds.ExecuteObjectCommand("showstep_controlpanel", "init");
        cp.refresh();
    }
}

/**
 * showstep_init is an entry point to initialize showstep's UI without
 * actually running showstep.
 */
function showstep_init () {
    showstep_ensure_init();
    the_show = new Show();
    (new ControlPanelUI()).refresh();
}
