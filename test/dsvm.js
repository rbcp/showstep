
const fs = require('fs');
const glob = require('glob');
const path = require('path');
const jsStringEscape = require('js-string-escape');
const {VM} = require('vm2');

var showstep_root = path.resolve(__dirname, '..');
var showstep_js_path = path.resolve(showstep_root, 'showstep.js');
var showstep_source = fs.readFileSync(showstep_js_path, 'utf8');

var dsvm = {
    DEBUG: false,
    ds_state: null,
    file_mocks: null,
    vm: null,
    add_file_mock: function (name, content) {
        dsvm.file_mocks[name] = content;
    },
    initialize: function () {
        dsvm.DEBUG = false;
        dsvm.file_mocks = {
            '$a.show': `{"playlist": [{ "section": "a" }]}`,
            '$b.show': `{"playlist": [{ "section": "a" },
                                      { "section": "b" }]}`,
            '$j.show': `{"playlist": [{ "section": "a" },
                                      { "section": "b" },
                                      { "section": "c" },
                                      { "section": "d" },
                                      { "section": "e" },
                                      { "section": "f" },
                                      { "section": "g" },
                                      { "section": "h" },
                                      { "section": "i" },
                                      { "section": "j" }]}`
        };
        dsvm.ds_state = {
            script: {
                play: __filename
            }
        };
        dsvm.vm = new VM({
            sandbox: {
                include: function (path) {
                    var source = fs.readFileSync(path, 'utf8');
                    dsvm.vm.run(source);
                },
                print: function (s) {
                    if (dsvm.DEBUG) {
                        console.log(s);
                    }
                },
                Ds: {
                    Wait: function (sec) {
                        // no waiting!
                    },
                    CreateObject: function (name, dsclass, type) {
                    },
                    NewObjectAttrRef: function (ob, attr) {
                        return [ob, attr];
                    },
                    GetObjectAttrUsingRef: function (ref) {
                        var [ob, attr] = ref;
                        if (ob in dsvm.ds_state) {
                            return dsvm.ds_state[ob][attr];
                        } else {
                            return undefined;
                        }
                    },
                    SetObjectAttrUsingRef: function (ref, val) {
                        var [ob, attr] = ref;
                        if (! (ob in dsvm.ds_state)) {
                            dsvm.ds_state[ob] = {};
                        }
                        dsvm.ds_state[ob][attr] = val;
                    },
                    AllocObjectArray: function (ob, attr, size) {
                        if (! (ob in dsvm.ds_state)) {
                            dsvm.ds_state[ob] = {};
                        }
                        dsvm.ds_state[ob][attr] = {};
                    },
                    GetObjectArrayElemUsingRef: function (ref, idx) {
                        var [ob, attr] = ref;
                        if ((ob in dsvm.ds_state) &&
                            (attr in dsvm.ds_state[ob]))
                        {
                            return dsvm.ds_state[ob][attr][idx];
                        } else {
                            return undefined;
                        }
                    },
                    SetObjectArrayElemUsingRef: function (ref, idx, val) {
                        var [ob, attr] = ref;
                        if (! (ob in dsvm.ds_state)) {
                            dsvm.ds_state[ob] = {};
                        }
                        if (! (attr in dsvm.ds_state[ob])) {
                            dsvm.ds_state[ob][attr] = {};
                        }
                        dsvm.ds_state[ob][attr][idx] = val;
                    },
                    DirectorySearch: function (path, globpat, recurse) {
                        var files = glob.sync(path+"/"+globpat);
                        return files;
                    },
                    FileAccess: function (pathname, mode) {
                        //XXX needs work
                        //  0: given access mode exists
                        //  13: Access denied
                        //  2: File name or path not found
                        //  22: Invalid parameter
                        if (fs.existsSync(pathname)) {
                            return 0;
                        } else {
                            return 1;
                        }
                    },
                    FileReadAllText: function (path, mode) {
                        if (path in dsvm.file_mocks) {
                            return dsvm.file_mocks[path];
                        } else {
                            try {
                                return fs.readFileSync(path, 'utf8');
                            } catch (e) {
                                if (e.code === 'ENOENT') {
                                    return -1;
                                } else {
                                    throw e;
                                }
                            }
                        }
                    },
                    ResolvePathName: function (path) {
                        //XXX: needs work
                        return path;
                    },
                    SendStringCommand: function (line) {
                        var line_trimmed = line.trim();
                        var m = line_trimmed.match(/^([^\s]*)\s+([^\s]*)\s*(.*)$/);
                        if (m) {
                            var ob = m[1];
                            var cmdattr = m[2];
                            var val = m[3];
                            if (val === undefined) {
                                val = "was called";
                            }
                            if (! (ob in dsvm.ds_state)) {
                                dsvm.ds_state[ob] = {};
                            }
                            if (val[0] == '"') {
                                val = val.substr(1);
                            }
                            if (val[val.length - 1] == '"') {
                                val = val.substr(0, val.length - 1);
                            }
                            dsvm.ds_state[ob][cmdattr] = val;
                        }
                    },
                    SendStringCommandBuffer: function (script) {
                        var script_sp = script.split("\n");
                        for (var i = 0, n = script_sp.length; i < n; ++i) {
                            var line_trimmed = script_sp[i].trim();
                            var m = line_trimmed.match(/^([^\s]*)\s+([^\s]*)\s*(.*)$/);
                            if (m) {
                                var ob = m[1];
                                var cmdattr = m[2];
                                var val = m[3];
                                if (val) {
                                    if (! (ob in dsvm.ds_state)) {
                                        dsvm.ds_state[ob] = {};
                                    }
                                    if (val[0] == '"') {
                                        val = val.substr(1);
                                    }
                                    if (val[val.length - 1] == '"') {
                                        val = val.substr(0, val.length - 1);
                                    }
                                    dsvm.ds_state[ob][cmdattr] = val;
                                }
                            }
                        }
                    },
                    SetTimerEvent: function () {
                    },
                    FileOpen: function () {
                    },
                    FileClose: function () {
                    },
                    FileWriteLine: function () {
                    }
                }
            }});

        dsvm.vm.run(showstep_source);

        var self_path_escaped = "'"+jsStringEscape(showstep_root)+"'";
        dsvm.vm.run("self_path = "+self_path_escaped+";");
        dsvm.vm.run("fx_load_path = join_paths(self_path, \"fx\");")
        dsvm.vm.run("dsrefs_init();")
        dsvm.vm.run("cp = new ControlPanelUI()");
    }
};

exports.dsvm = dsvm;
