
const dsvm = require('./dsvm').dsvm;
const expect = require('chai').expect;


/*
 * Tests
 */

describe("mocks", () => {
    beforeEach(dsvm.initialize)
    it("has controlpanel refs", () => {
        var controlpanel_currentsectionnotes_refs =
            dsvm.vm.run("cp.refs.notes");
        expect(controlpanel_currentsectionnotes_refs).to.be.an('array');
    })
    it("SendStringCommand records state that can be tested", () => {
        dsvm.vm.run('Ds.SendStringCommand("oristick on");');
        expect(dsvm.ds_state).to.include.key("oristick");
        expect(dsvm.ds_state.oristick).to.include.key("on");
    })
})

describe("show", () => {
    beforeEach(dsvm.initialize)
    it("null show has empty playlist", () => {
        var s = dsvm.vm.run("new Show();");
        expect(s.playlist.length()).to.equal(0);
    })
    it("default show playlist has index -1", () => {
        var s = dsvm.vm.run("new Show();");
        expect(s.playlist.index).to.equal(-1);
    })
    it("show with empty playlist has index -1", () => {
        dsvm.add_file_mock("$empty.show", `{
              "playlist": []
            }`);
        var s = dsvm.vm.run('new Show("$empty.show");');
        expect(s.playlist.index).to.equal(-1);
    })
    it("show with non-empty playlist starts on index -1", () => {
        var s = dsvm.vm.run('new Show("$a.show")');
        expect(s.playlist.index).to.equal(-1);
    })
    it("must have a playlist key", () => {
        dsvm.add_file_mock("$empty.show", `{}`);
        function tryit () {
            dsvm.vm.run('new Show("$empty.show");');
        }
        expect(tryit).to.throw();
    })
    it("can load a show file", () => {
        var s = dsvm.vm.run('new Show("./data/shows/a.show")');
        expect(s.playlist.length()).to.equal(1);
    })
    it("stores source property", () => {
        var s = dsvm.vm.run('new Show("./data/shows/a.show")');
        expect(s.source).to.include("data/shows/a.show");
    })
    it("supports section defaults", () => {
        dsvm.add_file_mock("$test.show", `{
              "defaults": {
                "hello": "world"
              },
              "playlist": [
                { "section": "a" }
              ]
            }`);
        var s = dsvm.vm.run('new Show("$test.show")');
        expect(s.playlist.get(0).hello).to.equal("world");
    })
    it("has fx_directory set", () => {
        var s = dsvm.vm.run('new Show("./data/shows/a.show")');
        expect(s.fx_directory).to.include("data/shows/scripts/fx");
    })
    it("'requires' are constructed at load time", () => {
        var s = dsvm.vm.run('new Show("./data/shows/require.show")');
        expect(s.effects["effects/require"]).to.be.a('object');
    })
    it("'requires' are scripted-toggles", () => {
        var s = dsvm.vm.run('var s = new Show("./data/shows/require.show");'+
                       's.effects["effects/require"] instanceof fx_scripted_toggle');
        expect(s).to.be.true;
    })
    it("next_section(false) simple case", () => {
        var s = dsvm.vm.run('new Show("$b.show")');
        expect(s.next_section(false)).to.be.a('object');
    })
    it("next_section(true) simple case", () => {
        var s = dsvm.vm.run('new Show("$b.show")');
        expect(s.next_section(true)).to.be.a('object');
    })
    it("next_section(true) out of nested show to another nested show", () => {
        var s = dsvm.vm.run(`
            the_show = new Show();
            the_show.playlist.insert(0,new Show("$a.show"));
            the_show.playlist.insert(1,new Show("$b.show"));
            the_show.playlist.index = 0;
            the_show.playlist.get(0).playlist.index = 0;
            the_show;`);
        var nxt = s.next_section(true);
        expect(nxt.show.source).to.equal("$b.show");
        expect(nxt.tag).to.equal("a");
    })
    it("constructs effects from its effects property", () => {
        dsvm.add_file_mock("$test.show", `{
            "effects": {
              "orion": "system-toggle:oristick"
            },
            "playlist": [
              { "section": "a",
                "require": ["@orion"] },
              { "section": "b",
                "require": ["@orion"] }
            ]
          }`);
        var s = dsvm.vm.run('new Show("$test.show")');
        expect(s.effects).to.be.an('object')
        expect(s.effects).to.include.key('orion')
        expect(s.effects.orion).to.include.key('name')
        expect(s.effects.orion.name).to.equal('orion')
    })
    it("is an error to have '@' refs in effects section", () => {
        dsvm.add_file_mock("$test.show", `{
            "effects": {
              "orion": "@orion"
            },
            "playlist": []
          }`);
        function tryit () {
            dsvm.vm.run('new Show("$test.show")');
        }
        expect(tryit).to.throw();
    })
    it("show.autoplay is a property", () => {
        dsvm.add_file_mock("$test.show", `{
              "autoplay": true,
              "playlist": [
                { "section": "a" }
              ]
            }`);
        var s = dsvm.vm.run('new Show("$test.show")');
        expect(s.autoplay).to.be.true;
    })
    it("show.autoplay is true if first section is autoplay", () => {
        dsvm.add_file_mock("$test.show", `{
              "playlist": [
                { "section": "a",
                  "autoplay": true }
              ]
            }`);
        var s = dsvm.vm.run('new Show("$test.show")');
        expect(s.autoplay).to.be.true;
    })
    it("throws if playlist is not an array", () => {
        dsvm.add_file_mock("$test.show", `{"playlist": {}}`);
        expect(() => { dsvm.vm.run('new Show("$test.show")') }).to.throw();
    })
    it("menu item causes ref to be added to section use_effects", () => {
        dsvm.add_file_mock("$test.show", `{
              "playlist": [
                { "section": "a" }
              ],
              "menus": {
                "a": {
                  "1": "Hello::system-toggle:oristick"
                }
              }
            }`);
        var s = dsvm.vm.run(`new Show("$test.show");`);
        expect(s.playlist.get(0).use_effects).to.include.key("system-toggle0");
    })
    it("menu for non-section still gets constructed", () => {
        dsvm.add_file_mock("$test.show", `{
              "playlist": [
                { "section": "a" }
              ],
              "menus": {
                "b": {
                  "1": "Hello::system-toggle:oristick"
                }
              }
            }`);
        var s = dsvm.vm.run(`new Show("$test.show");`);
        expect(s.menus).to.include.key("b");
    })
    it("can load alternate playlist with `|' syntax", () => {
        dsvm.add_file_mock("$test.show", `{
              "playlists": {
                "hello": [{ "section": "b" }]
              },
              "playlist": [
                { "section": "a" }
              ]
            }`);
        var s = dsvm.vm.run('new Show("$test.show|hello")');
        expect(s.playlist.get(0).tag).to.equal("b");
    })
});

describe("show (from script sequence)", () => {
    beforeEach(dsvm.initialize)
    it("given script filepath, has a playlist", () => {
        var s = dsvm.vm.run('new Show("./data/sequences/a/scripts/01.first.ds")');
        expect(s.playlist).to.be.a('object');
    })
    it("given 'showroot|section-name', has a playlist", () => {
        var s = dsvm.vm.run('new Show("./data/sequences/a|first")');
        expect(s.playlist).to.be.a('object');
    })
    it("given 'showroot', has a playlist with entries", () => {
        var s = dsvm.vm.run('new Show("./data/sequences/a")');
        expect(s.playlist.length()).to.equal(3);
    })
    it("stores source property", () => {
        var s = dsvm.vm.run('new Show("./data/sequences/a|first")');
        expect(s.source).to.include("data/sequences/a");
    })
    it("given script filepath, has correct section filename", () => {
        var s = dsvm.vm.run('new Show("./data/sequences/a/scripts/01.first.ds")');
        expect(s.playlist.get(0).script_play).to.equal("01.first.ds");
    })
});

describe("show (from standalone section)", () => {
    beforeEach(dsvm.initialize)
    it("given -play script, has a playlist", () => {
        var s = dsvm.vm.run('new Show("./data/standalones/a/a-play.ds")');
        expect(s.playlist.length()).to.equal(1);
    })
    it("given directory containing -play script, has a playlist", () => {
        var s = dsvm.vm.run('new Show("./data/standalones/a")');
        expect(s.playlist.length()).to.equal(1);
    })
})

describe("section", () => {
    beforeEach(dsvm.initialize)
    it("loads properties sidecar file with standalone play script", () => {
        var s = dsvm.vm.run('new Show("./data/standalones/autoplay/autoplay-play.ds")');
        expect(s.playlist.get(0).autoplay).to.be.true;
    })
    it("loads properties sidecar file with standalone directory", () => {
        var s = dsvm.vm.run('new Show("./data/standalones/autoplay/")');
        expect(s.playlist.get(0).autoplay).to.be.true;
    })
})

describe("show notes", () => {
    beforeEach(dsvm.initialize)

    it("is an empty object when no notes are given in the spec", () => {
        var s = dsvm.vm.run('new Show("$a.show")');
        expect(s.notes).to.be.an('object').that.is.empty
    })
    it("loads show-notes.json", () => {
        var s = dsvm.vm.run('new Show("./data/sequences/a")');
        expect(s.notes).to.be.an('object').that.has.keys('first','second','third');
    })


    //XXX test that show.notes is the authoritative version of the notes.
    //    notes may be given in individual sections, but those will be merged
    //    into the master list which is a property of the show.
    //
    //    also if you update_notes, make sure the authoritative version
    //    goes into the master notes list of the show.
    //
})

describe("insert", () => {
    beforeEach(dsvm.initialize)

    it("a.show[0] insert b.show", () => {
        var s = dsvm.vm.run('the_show = new Show("$a.show");'+
                       'Ds.SetObjectAttrUsingRef(dsrefs.showstep.insert, "$b.show");'+
                       'showstep_insert_callback();'+
                       'the_show');
        expect(s.playlist.length()).to.equal(2);
    })
    it("nested show has parent property", () => {
        var s = dsvm.vm.run('the_show = new Show("$a.show");'+
                            'the_show.playlist.index = 0;'+
                       'Ds.SetObjectAttrUsingRef(dsrefs.showstep.insert, "$b.show");'+
                       'showstep_insert_callback();'+
                       'the_show');
        expect(s.playlist.get(1).parent).to.equal(s);
    })
    it("can insert into a nested show", () => {
        dsvm.add_file_mock("$main.show", `{
              "playlist": [
                {"section": "walk-in"},
                {"section": "introduction"}]}`);
        dsvm.add_file_mock("$nested1.show", `{
              "playlist": [{"section": "nested1"}]}`);
        dsvm.add_file_mock("$nested2.show", `{
              "playlist": [{"section": "nested2"}]}`);
        var s = dsvm.vm.run(`
                     the_show = new Show("$main.show");
                     the_show.playlist.index = 0;
                     Ds.SetObjectAttrUsingRef(dsrefs.showstep.insert, "$nested1.show");
                     showstep_insert_callback();
                     the_show.playlist.index = 1; // put us in first nested show
                     Ds.SetObjectAttrUsingRef(dsrefs.showstep.insert, "$nested2.show");
                     showstep_insert_callback();
                     the_show;`);
        expect(s.playlist.length()).to.be.equal(3);
        expect(s.playlist.get(1).playlist.length()).to.be.equal(2);
    })
    it("cannot insert into an autoplay show", () => {
        dsvm.add_file_mock("$main.show", `{
              "playlist": [
                {"section": "walk-in"},
                {"section": "introduction"}]}`);
        dsvm.add_file_mock("$vignette1.show", `{
              "autoplay": true,
              "playlist": [{"section": "vignette1"}]}`);
        dsvm.add_file_mock("$vignette2.show", `{
              "autoplay": true,
              "playlist": [{"section": "vignette2"}]}`);
        var s = dsvm.vm.run(`
                     the_show = new Show("$main.show");
                     the_show.playlist.index = 0;
                     Ds.SetObjectAttrUsingRef(dsrefs.showstep.insert, "$vignette1.show");
                     showstep_insert_callback();
                     // we should be in the first vignette because of autoplay
                     // but we will be explicit about it for test's sake.
                     //
                     the_show.playlist.index = 1; // put us in first vignette
                     the_show.playlist.get(1).playlist.index = 0;
                     Ds.SetObjectAttrUsingRef(dsrefs.showstep.insert, "$vignette2.show");
                     showstep_insert_callback();
                     the_show;`);
        expect(s.playlist.length()).to.be.equal(4);
    })
    it("can still insert if root show is autoplay (edge case)", () => {
        dsvm.add_file_mock("$main.show", `{
              "autoplay": true,
              "playlist": [
                {"section": "walk-in"},
                {"section": "introduction"}]}`);
        dsvm.add_file_mock("$vignette1.show", `{
              "autoplay": true,
              "playlist": [{"section": "vignette1"}]}`);
        var s = dsvm.vm.run(`
                     the_show = new Show("$main.show");
                     Ds.SetObjectAttrUsingRef(dsrefs.showstep.insert, "$vignette1.show");
                     showstep_insert_callback();
                     the_show;`);
        expect(s.playlist.length()).to.be.equal(3);
    })
    it("cannot insert same show more than once", () => {
        dsvm.add_file_mock("$main.show", `{
              "playlist": [
                {"section": "walk-in"},
                {"section": "introduction"}]}`);
        dsvm.add_file_mock("$nested1.show", `{
              "playlist": [{"section": "nested1"}]}`);
        var s = dsvm.vm.run(`
                     the_show = new Show("$main.show");
                     Ds.SetObjectAttrUsingRef(dsrefs.showstep.insert, "$nested1.show");
                     showstep_insert_callback();
                     Ds.SetObjectAttrUsingRef(dsrefs.showstep.insert, "$nested1.show");
                     showstep_insert_callback();
                     the_show;`);
        expect(s.playlist.length()).to.be.equal(3);
    })
    it("cannot insert same show more than once (autoplay case)", () => {
        dsvm.add_file_mock("$main.show", `{
              "playlist": [
                {"section": "walk-in"},
                {"section": "introduction"}]}`);
        dsvm.add_file_mock("$nested1.show", `{
              "autoplay": true,
              "playlist": [{"section": "nested1"}]}`);
        dsvm.add_file_mock("$nested2.show", `{
              "autoplay": true,
              "playlist": [{"section": "nested2"}]}`);
        var s = dsvm.vm.run(`
                     the_show = new Show("$main.show");
                     Ds.SetObjectAttrUsingRef(dsrefs.showstep.insert, "$nested1.show");
                     showstep_insert_callback();
                     Ds.SetObjectAttrUsingRef(dsrefs.showstep.insert, "$nested2.show");
                     showstep_insert_callback();
                     Ds.SetObjectAttrUsingRef(dsrefs.showstep.insert, "$nested1.show");
                     showstep_insert_callback();
                     the_show;`);
        expect(s.playlist.length()).to.be.equal(4);
    })
    it("updates parent indices on autoplay insertion", () => {
        dsvm.add_file_mock("$main.show", `{
              "playlist": [
                {"section": "walk-in"},
                {"section": "introduction"}]}`);
        dsvm.add_file_mock("$nested1.show", `{
              "autoplay": true,
              "playlist": [{"section": "nested1"}]}`);
        var Show = dsvm.vm.run('Show;');
        var s = dsvm.vm.run(`
                    the_show = new Show("$main.show");
                    the_show.playlist.index = 0;
                    Ds.SetObjectAttrUsingRef(dsrefs.showstep.insert,
                                             "$nested1.show");
                    showstep_insert_callback();
                    the_show;`);
        expect(s.playlist.get(1)).to.be.an.instanceof(Show)
        expect(s.playlist.get(1).playlist.index).to.equal(0);
        expect(s.playlist.index).to.equal(1);
    })
    it("insert two autoplays and both play", () => {
        dsvm.add_file_mock("$main.show", `{
              "playlist": [
                {"section": "walk-in"},
                {"section": "introduction"}]}`);
        dsvm.add_file_mock("$vignette1.show", `{
              "autoplay": true,
              "playlist": [{"section": "vignette1", "lock": 0}]}`);
        dsvm.add_file_mock("$vignette2.show", `{
              "autoplay": true,
              "playlist": [{"section": "vignette2", "lock": 0}]}`);
        var s = dsvm.vm.run(`
                     the_show = new Show("$main.show");
                     the_show.playlist.index = 0;
                     var v1 = new Show("$vignette1.show");
                     the_show.playlist.insert(1, v1);
                     var v2 = new Show("$vignette2.show");
                     the_show.playlist.insert(2, v2);
                     section_play(the_show.next_section(true));
                     the_show;`);
        expect(s.playlist.length()).to.equal(4);
        expect(s.playlist.index).to.equal(2);
    })
});

describe("show.cleanup_list_for_section", () => {
    beforeEach(dsvm.initialize)

    function section_tag (x) {
        return x.section.tag;
    }

    it("advance to first section", () => {
        dsvm.add_file_mock("$test.show", `{"playlist": [
                { "section": "a" }
              ]}`);
        var s = dsvm.vm.run('the_show = new Show("$test.show");');
        var lst = dsvm.vm.run('the_show.cleanup_list_for_section(the_show.playlist.get(0));');
        expect(lst).to.be.an('array').that.is.empty;
    })
    it("advance to child", () => {
        dsvm.add_file_mock("$test.show", `{"playlist": [
                { "section": "a" },
                { "section": "a.b" }
              ]}`);
        var s = dsvm.vm.run('the_show = new Show("$test.show");');
        var lst = dsvm.vm.run('the_show.cleanup_list_for_section(the_show.playlist.get(1));');
        expect(lst).to.be.an('array').that.is.empty;
    })
    it("advance to grandchild", () => {
        dsvm.add_file_mock("$test.show", `{"playlist": [
                { "section": "a" },
                { "section": "a.b.c" }
              ]}`);
        var s = dsvm.vm.run('the_show = new Show("$test.show");');
        var lst = dsvm.vm.run('the_show.cleanup_list_for_section(the_show.playlist.get(1));');
        expect(lst).to.be.an('array').that.is.empty;
    })
    it("advance to sibling0", () => {
        dsvm.add_file_mock("$test.show", `{"playlist": [
                { "section": "a" },
                { "section": "b" }
              ]}`);
        var s = dsvm.vm.run('the_show = new Show("$test.show");');
        var lst = dsvm.vm.run('the_show.cleanup_list_for_section(the_show.playlist.get(1));');
        expect(lst.map(section_tag)).to.deep.equal([
            "a"
        ]);
    })
    it("advance to sibling1", () => {
        dsvm.add_file_mock("$test.show", `{"playlist": [
                { "section": "a" },
                { "section": "a.b" },
                { "section": "a.c" }
              ]}`);
        var s = dsvm.vm.run('the_show = new Show("$test.show");');
        var lst = dsvm.vm.run('the_show.cleanup_list_for_section(the_show.playlist.get(2));');
        expect(lst.map(section_tag)).to.deep.equal([
            "a.b"
        ]);
    })
    it("advance up1", () => {
        dsvm.add_file_mock("$test.show", `{"playlist": [
                { "section": "a" },
                { "section": "a.b" },
                { "section": "c" }
              ]}`);
        var s = dsvm.vm.run('the_show = new Show("$test.show");');
        var lst = dsvm.vm.run('the_show.cleanup_list_for_section(the_show.playlist.get(2));');
        expect(lst.map(section_tag)).to.deep.equal([
            "a.b", "a"
        ]);
    })
    it("advance up2", () => {
        dsvm.add_file_mock("$test.show", `{"playlist": [
                { "section": "a" },
                { "section": "a.b" },
                { "section": "a.b.c" },
                { "section": "d" }
              ]}`);
        var s = dsvm.vm.run('the_show = new Show("$test.show");');
        var lst = dsvm.vm.run('the_show.cleanup_list_for_section(the_show.playlist.get(3));');
        expect(lst.map(section_tag)).to.deep.equal([
            "a.b.c", "a.b", "a"
        ]);
    })
    it("advance second sibling", () => {
        dsvm.add_file_mock("$test.show", `{"playlist": [
                { "section": "a" },
                { "section": "a.b" },
                { "section": "a.c" },
                { "section": "a.d" }
              ]}`);
        var s = dsvm.vm.run('the_show = new Show("$test.show");');
        var lst = dsvm.vm.run('the_show.cleanup_list_for_section(the_show.playlist.get(3));');
        expect(lst.map(section_tag)).to.deep.equal([
            "a.c"
        ]);
    })
    it("advance third sibling", () => {
        dsvm.add_file_mock("$test.show", `{"playlist": [
                { "section": "a" },
                { "section": "a.b" },
                { "section": "a.c" },
                { "section": "a.d" },
                { "section": "a.e" }
              ]}`);
        var s = dsvm.vm.run('the_show = new Show("$test.show");');
        var lst = dsvm.vm.run('the_show.cleanup_list_for_section(the_show.playlist.get(4));');
        expect(lst.map(section_tag)).to.deep.equal([
            "a.d"
        ]);
    })
    it("advance up2 skip1", () => {
        dsvm.add_file_mock("$test.show", `{"playlist": [
                { "section": "a" },
                { "section": "a.b" },
                { "section": "a.b.c" },
                { "section": "a.b.d" },
                { "section": "e" }
              ]}`);
        var s = dsvm.vm.run('the_show = new Show("$test.show");');
        var lst = dsvm.vm.run('the_show.cleanup_list_for_section(the_show.playlist.get(4));');
        expect(lst.map(section_tag)).to.deep.equal([
            "a.b.d", "a.b", "a"
        ]);
    })
    it("advance to third", () => {
        dsvm.add_file_mock("$test.show", `{"playlist": [
                { "section": "a" },
                { "section": "b" },
                { "section": "c" }
              ]}`);
        var s = dsvm.vm.run('the_show = new Show("$test.show");');
        var lst = dsvm.vm.run('the_show.cleanup_list_for_section(the_show.playlist.get(2));');
        expect(lst.map(section_tag)).to.deep.equal([
            "b"
        ]);
    })
    it("star-lore 1", () => {
        dsvm.add_file_mock("$test.show", `{"playlist": [
                { "section": "walk-in" },
                { "section": "introduction" },
                { "section": "introduction.intro-greek" },
                { "section": "introduction.intro-ojibwe" },
                { "section": "introduction.intro-chinese" },
                { "section": "northern-sky" }
              ]}`);
        var s = dsvm.vm.run('the_show = new Show("$test.show");');
        var lst = dsvm.vm.run('the_show.cleanup_list_for_section(the_show.playlist.get(1));');
        expect(lst.map(section_tag)).to.deep.equal([
            "walk-in"
        ]);
    })
    it("star-lore 2", () => {
        dsvm.add_file_mock("$test.show", `{"playlist": [
                { "section": "walk-in" },
                { "section": "introduction" },
                { "section": "introduction.intro-greek" },
                { "section": "introduction.intro-ojibwe" },
                { "section": "introduction.intro-chinese" },
                { "section": "northern-sky" }
              ]}`);
        var s = dsvm.vm.run('the_show = new Show("$test.show");');
        var lst = dsvm.vm.run('the_show.cleanup_list_for_section(the_show.playlist.get(2));');
        expect(lst.map(section_tag)).to.be.empty;
    })
    it("star-lore 3", () => {
        dsvm.add_file_mock("$test.show", `{"playlist": [
                { "section": "walk-in" },
                { "section": "introduction" },
                { "section": "introduction.intro-greek" },
                { "section": "introduction.intro-ojibwe" },
                { "section": "introduction.intro-chinese" },
                { "section": "northern-sky" }
              ]}`);
        var s = dsvm.vm.run('the_show = new Show("$test.show");');
        var lst = dsvm.vm.run('the_show.cleanup_list_for_section(the_show.playlist.get(3));');
        expect(lst.map(section_tag)).to.deep.equal([
            "introduction.intro-greek"
        ]);
    })
    it("star-lore 4", () => {
        dsvm.add_file_mock("$test.show", `{"playlist": [
                { "section": "walk-in" },
                { "section": "introduction" },
                { "section": "introduction.intro-greek" },
                { "section": "introduction.intro-ojibwe" },
                { "section": "introduction.intro-chinese" },
                { "section": "northern-sky" }
              ]}`);
        var s = dsvm.vm.run('the_show = new Show("$test.show");');
        var lst = dsvm.vm.run('the_show.cleanup_list_for_section(the_show.playlist.get(4));');
        expect(lst.map(section_tag)).to.deep.equal([
            "introduction.intro-ojibwe"
        ]);
    })
    it("star-lore 5", () => {
        dsvm.add_file_mock("$test.show", `{"playlist": [
                { "section": "walk-in" },
                { "section": "introduction" },
                { "section": "introduction.intro-greek" },
                { "section": "introduction.intro-ojibwe" },
                { "section": "introduction.intro-chinese" },
                { "section": "northern-sky" }
              ]}`);
        var s = dsvm.vm.run('the_show = new Show("$test.show");');
        var lst = dsvm.vm.run('the_show.cleanup_list_for_section(the_show.playlist.get(5));');
        expect(lst.map(section_tag)).to.deep.equal([
            "introduction.intro-chinese", "introduction"
        ]);
    })
    it("advance to sibling without parent", () => {
        dsvm.add_file_mock("$test.show", `{"playlist": [
                { "section": "a" },
                { "section": "b.c" },
                { "section": "b.d" }
              ]}`);
        var s = dsvm.vm.run('the_show = new Show("$test.show");');
        var lst = dsvm.vm.run('the_show.cleanup_list_for_section(the_show.playlist.get(2));');
        expect(lst.map(section_tag)).to.deep.equal([
            "b.c"
        ]);
    })
    it("advance to sibling without parent tricky", () => {
        dsvm.add_file_mock("$test.show", `{"playlist": [
                { "section": "b" },
                { "section": "bb.c" },
                { "section": "bb.d" }
              ]}`);
        var s = dsvm.vm.run('the_show = new Show("$test.show");');
        var lst = dsvm.vm.run('the_show.cleanup_list_for_section(the_show.playlist.get(2));');
        expect(lst.map(section_tag)).to.deep.equal([
            "bb.c"
        ]);
    })
    it("advance to sibling of complex tree 1", () => {
        dsvm.add_file_mock("$test.show", `{"playlist": [
                { "section": "b" },
                { "section": "b.c" },
                { "section": "b.d.e" },
                { "section": "z" }
              ]}`);
        var s = dsvm.vm.run('the_show = new Show("$test.show");');
        var lst = dsvm.vm.run('the_show.cleanup_list_for_section(the_show.playlist.get(3));');
        expect(lst.map(section_tag)).to.deep.equal([
            "b.d.e", "b"
        ]);
    })
    it("advance to sibling of complex tree 2", () => {
        dsvm.add_file_mock("$test.show", `{"playlist": [
                { "section": "a.b" },
                { "section": "a.b.c" },
                { "section": "a.b.d.e" },
                { "section": "a.z" }
              ]}`);
        var s = dsvm.vm.run('the_show = new Show("$test.show");');
        var lst = dsvm.vm.run('the_show.cleanup_list_for_section(the_show.playlist.get(3));');
        expect(lst.map(section_tag)).to.deep.equal([
            "a.b.d.e", "a.b"
        ]);
    })
    it("advance to sibling of complex tree 3", () => {
        dsvm.add_file_mock("$test.show", `{"playlist": [
                { "section": "a.b" },
                { "section": "a.b.c" },
                { "section": "a.b.d.e" },
                { "section": "a.b.d.f" },
                { "section": "a.b.d.g" },
                { "section": "a.h" }
              ]}`);
        var s = dsvm.vm.run('the_show = new Show("$test.show");');
        var lst = dsvm.vm.run('the_show.cleanup_list_for_section(the_show.playlist.get(5));');
        expect(lst.map(section_tag)).to.deep.equal([
            "a.b.d.g", "a.b"
        ]);
    })
    it("advance off end of list", () => {
        dsvm.add_file_mock("$test.show", `{"playlist": [
                { "section": "a" }
              ]}`);
        var s = dsvm.vm.run('the_show = new Show("$test.show");');
        var lst = dsvm.vm.run('the_show.cleanup_list_for_section(the_show.playlist.get(1));');
        expect(lst.map(section_tag)).to.deep.equal(["a"]);
    })
    it("advance off end of list 2", () => {
        dsvm.add_file_mock("$test.show", `{"playlist": [
                { "section": "a" },
                { "section": "a.b" }
              ]}`);
        var s = dsvm.vm.run('the_show = new Show("$test.show");');
        var lst = dsvm.vm.run('the_show.cleanup_list_for_section(the_show.playlist.get(2));');
        expect(lst.map(section_tag)).to.deep.equal(["a.b", "a"]);
    })
    it("advance off end of child show to section in parent", () => {
        dsvm.add_file_mock("$vignette.show", `{"playlist": [
                { "section": "vignette1" },
                { "section": "vignette2" }
              ]}`);
        var lst = dsvm.vm.run(`
            the_show = new Show("$b.show");
            the_show.playlist.insert(1,new Show("$vignette.show"));
            the_show.playlist.index = 1;
            the_show.playlist.get(1).playlist.index = 1;
            the_show.cleanup_list_for_section(the_show.playlist.get(2));
            `);
        expect(lst.map(section_tag)).to.deep.equal(["vignette2", "a"]);
    })
    it("advance off end of child show to end of the_show", () => {
        var lst = dsvm.vm.run(`
            the_show = new Show();
            the_show.playlist.insert(0,new Show("$b.show"));
            the_show.playlist.index = 0;
            the_show.playlist.get(0).playlist.index = 0;
            the_show.cleanup_list_for_section(null);
            `);
        expect(lst.map(section_tag)).to.deep.equal(["b"]);
    })
    it("works on any show, not just the_show", () => {
        dsvm.add_file_mock("$test.show", `{"playlist": [
                { "section": "a" },
                { "section": "b" }
              ]}`);
        var s = dsvm.vm.run('some_show = new Show("$test.show");');
        var lst = dsvm.vm.run('some_show.cleanup_list_for_section(some_show.playlist.get(1));');
        expect(lst.map(section_tag)).to.deep.equal(["a"]);
    })
    it("ignores nested shows other than the last", () => {
        dsvm.add_file_mock("$main.show", `{
              "playlist": [
                {"section": "walk-in"},
                {"section": "introduction"}]}`);
        dsvm.add_file_mock("$vignette1.show", `{
              "autoplay": true,
              "playlist": [{"section": "vignette1", "lock": 0}]}`);
        dsvm.add_file_mock("$vignette2.show", `{
              "autoplay": true,
              "playlist": [{"section": "vignette2", "lock": 0}]}`);
        var lst = dsvm.vm.run(`
                     the_show = new Show("$main.show");
                     the_show.playlist.index = 0;
                     var v1 = new Show("$vignette1.show");
                     the_show.playlist.insert(1, v1);
                     var v2 = new Show("$vignette2.show");
                     the_show.playlist.insert(2, v2);
                     the_show.cleanup_list_for_section(the_show.playlist.get(3));`);
        expect(lst.map(section_tag)).to.deep.equal(["vignette2", "walk-in"]);
    })
})

describe("show.cleanup_list_for_section topic popping support", () => {
    beforeEach(dsvm.initialize)

    function section_tag (x) {
        return x.section.tag;
    }

    it("pop", () => {
        dsvm.add_file_mock("$test.show", `{"playlist": [
                { "section": "a.b" },
                { "section": "a" }
              ]}`);
        var s = dsvm.vm.run('the_show = new Show("$test.show");');
        var lst = dsvm.vm.run('the_show.cleanup_list_for_section(the_show.playlist.get(1));');
        expect(lst.map(section_tag)).to.deep.equal([
            "a.b"
        ]);
    })
    it("pop repeat", () => {
        dsvm.add_file_mock("$test.show", `{"playlist": [
                { "section": "a" },
                { "section": "a.b" },
                { "section": "a" }
              ]}`);
        var s = dsvm.vm.run('the_show = new Show("$test.show");');
        var lst = dsvm.vm.run('the_show.cleanup_list_for_section(the_show.playlist.get(2));');
        expect(lst.map(section_tag)).to.deep.equal([
            "a.b"
        ]);
    })
    it("repeat section", () => {
        dsvm.add_file_mock("$test.show", `{"playlist": [
                { "section": "a" },
                { "section": "a" }
              ]}`);
        var s = dsvm.vm.run('the_show = new Show("$test.show");');
        var lst = dsvm.vm.run('the_show.cleanup_list_for_section(the_show.playlist.get(1));');
        expect(lst.map(section_tag)).to.be.empty;
    })
    it("repeat section 2", () => {
        dsvm.add_file_mock("$test.show", `{"playlist": [
                { "section": "a" },
                { "section": "a" },
                { "section": "b" }
              ]}`);
        var s = dsvm.vm.run('the_show = new Show("$test.show");');
        var lst = dsvm.vm.run('the_show.cleanup_list_for_section(the_show.playlist.get(2));');
        expect(lst.map(section_tag)).to.deep.equal([
            "a"
        ]);
    })

})

// describe("button effects cleanup", () => {
//     beforeEach(dsvm.initialize)

//     describe("", () => {
//     })

// })

describe("control panel labels", () => {
    beforeEach(dsvm.initialize)
    it("sets correct next section", () => {
        dsvm.add_file_mock("$main.show", `{
              "playlist": [
                {"section": "walk-in"},
                {"section": "introduction"}]}`);
        var lst = dsvm.vm.run(`
            the_show = new Show("$main.show");
            showstep_next_callback();
            `);
        expect(dsvm.ds_state).to.include.key('showstep_controlpanel');
        expect(dsvm.ds_state.showstep_controlpanel).to.include.key('nextsection');
        expect(dsvm.ds_state.showstep_controlpanel.nextsection).to.equal("introduction");
    })
})

describe("control panel playlist", () => {
    function get_buttons_state () {
        // current section indicated by a star
        var got = [1,2,3,4,5,6].map(function (i) {
            var cur = dsvm.ds_state["showstep_playlist_button"+i].color != "0 0 0";
            return (cur ? "*" : "")+dsvm.ds_state["showstep_playlist_button"+i].label;
        });
        return got;
    }

    beforeEach(dsvm.initialize)

    it("sets labels for exactly six playlist buttons", () => {
        var s = dsvm.vm.run('the_show = new Show("$a.show");'+
                            'cp.playlist_update();');
        expect(dsvm.ds_state).not.to.include.key("showstep_playlist_button0");
        expect(dsvm.ds_state).to.include.key("showstep_playlist_button1");
        expect(dsvm.ds_state).to.include.key("showstep_playlist_button2");
        expect(dsvm.ds_state).to.include.key("showstep_playlist_button3");
        expect(dsvm.ds_state).to.include.key("showstep_playlist_button4");
        expect(dsvm.ds_state).to.include.key("showstep_playlist_button5");
        expect(dsvm.ds_state).to.include.key("showstep_playlist_button6");
        expect(dsvm.ds_state).not.to.include.key("showstep_playlist_button7");
        expect(dsvm.ds_state.showstep_playlist_button1).to.include.key("label");
        expect(dsvm.ds_state.showstep_playlist_button2).to.include.key("label");
        expect(dsvm.ds_state.showstep_playlist_button3).to.include.key("label");
        expect(dsvm.ds_state.showstep_playlist_button4).to.include.key("label");
        expect(dsvm.ds_state.showstep_playlist_button5).to.include.key("label");
        expect(dsvm.ds_state.showstep_playlist_button6).to.include.key("label");
    })
    it("sets labels for a.show index -1", () => {
        var s = dsvm.vm.run('the_show = new Show("$a.show");'+
                       'cp.playlist_update();');
        expect(get_buttons_state()).to.deep.equal(
            ['a','','','','','']);
    })
    it("sets labels for b.show index -1", () => {
        var s = dsvm.vm.run('the_show = new Show("$b.show");'+
                       'cp.playlist_update();');
        expect(get_buttons_state()).to.deep.equal(
            ['a','b','','','','']);
    })
    it("sets labels for b.show index 1", () => {
        var s = dsvm.vm.run('the_show = new Show("$b.show");'+
                       'the_show.playlist.index = 1;'+
                       'cp.playlist_update();');
        expect(get_buttons_state()).to.deep.equal(
            ['a','*b','','','','']);
    })
    it("scrolls down", () => {
        var s = dsvm.vm.run('the_show = new Show("$j.show");'+
                       'the_show.playlist.index = 3;'+
                       'cp.playlist_scroll_down();');
        expect(get_buttons_state()).to.deep.equal(
            ['*d','e','f','g','h','i']);
    })
    it("scrolls up", () => {
        var s = dsvm.vm.run('the_show = new Show("$j.show");'+
                       'the_show.playlist.index = 3;'+
                       'cp.playlist_scroll_up();');
        expect(get_buttons_state()).to.deep.equal(
            ['b','c','*d','e','f','g']);
    })
    it("scrolls down two", () => {
        var s = dsvm.vm.run(`
                    the_show = new Show("$j.show");
                    the_show.playlist.index = 3;
                    cp.playlist_scroll_down();
                    cp.playlist_scroll_down();`);
        expect(get_buttons_state()).to.deep.equal(
            ['e','f','g','h','i','j']);
    })
    it("scrolls down three", () => {
        var s = dsvm.vm.run(`
                    the_show = new Show("$j.show");
                    the_show.playlist.index = 2;
                    cp.playlist_scroll_down();
                    cp.playlist_scroll_down();
                    cp.playlist_scroll_down();`);
        expect(get_buttons_state()).to.deep.equal(
            ['e','f','g','h','i','j']);
    })
    it("does not scroll down off end", () => {
        var s = dsvm.vm.run(`
                    the_show = new Show("$j.show");
                    the_show.playlist.index = 3;
                    cp.playlist_scroll_down();
                    cp.playlist_scroll_down();
                    cp.playlist_scroll_down();`);
        expect(get_buttons_state()).to.deep.equal(
            ['e','f','g','h','i','j']);
    })
    it("scrolls up two", () => {
        var s = dsvm.vm.run(`
                    the_show = new Show("$j.show");
                    the_show.playlist.index = 3;
                    cp.playlist_scroll_up();
                    cp.playlist_scroll_up();`);
        expect(get_buttons_state()).to.deep.equal(
            ['a','b','c','*d','e','f']);
    })
    it("scroll can be reset", () => {
        var s = dsvm.vm.run('the_show = new Show("$j.show");'+
                       'the_show.playlist.index = 3;'+
                       'cp.playlist_scroll_up();'+
                       'cp.playlist_scroll_reset();');
        expect(get_buttons_state()).to.deep.equal(
            ['c','*d','e','f','g','h']);
    })
    it("a.show[0] insert b.show", () => {
        dsvm.vm.run(`the_show = new Show("$a.show");
                     the_show.playlist.index = 0;
                     Ds.SetObjectAttrUsingRef(dsrefs.showstep.insert, "$b.show");
                     showstep_insert_callback();`);
        expect(get_buttons_state()).to.deep.equal(
            ['*a','a','b','','','']);
    })
    it("continues listing after nested show", () => {
        dsvm.vm.run(`the_show = new Show();
                     Ds.SetObjectAttrUsingRef(dsrefs.showstep.insert, "$b.show");
                     showstep_insert_callback();
                     Ds.SetObjectAttrUsingRef(dsrefs.showstep.insert, "$a.show");
                     showstep_insert_callback();`);
        expect(get_buttons_state()).to.deep.equal(
            ['a','a','b','','','']);
    })
    it("autoplay inserted into an empty show, playlist starts at top", () => {
        dsvm.add_file_mock("$nested1.show", `{
              "autoplay": true,
              "playlist": [{"section": "nested1"}]}`);
        dsvm.vm.run(`the_show = new Show();
                     Ds.SetObjectAttrUsingRef(dsrefs.showstep.insert,
                                              "$nested1.show");
                     showstep_insert_callback();
                     the_show;`);
        expect(get_buttons_state()).to.deep.equal(
            ['*nested1','','','','','']);
    })
    it("position with multiple nested shows", () => {
        dsvm.add_file_mock("$main.show", `{
              "playlist": [
                {"section": "walk-in"},
                {"section": "introduction"}]}`);
        dsvm.add_file_mock("$vignette1.show", `{
              "playlist": [{"section": "vignette1", "lock": 0}]}`);
        dsvm.add_file_mock("$vignette2.show", `{
              "playlist": [{"section": "vignette2", "lock": 0}]}`);
        dsvm.vm.run(`the_show = new Show("$main.show");
                     the_show.playlist.index = 0;
                     var v1 = new Show("$vignette1.show");
                     the_show.playlist.insert(1, v1);
                     var v2 = new Show("$vignette2.show");
                     the_show.playlist.insert(2, v2);
                     the_show.playlist.index = 2;
                     the_show.playlist.get(2).playlist.index = 0;
                     cp.playlist_update();`);
        expect(get_buttons_state()).to.deep.equal(
            ['vignette1','*vignette2','introduction','','','']);
    })
})

describe("runtime menus", () => {
    beforeEach(dsvm.initialize)
    it("includes runtime button effects in cleanup", () => {
        dsvm.vm.run(`the_show = new Show("$b.show");
                     section_play(the_show.playlist.get(0), true);
                     Ds.SetObjectArrayElemUsingRef(dsrefs.showstep.button, 1,
                                                   "Test::system-toggle:oristick");
                     showstep_button_callback_suppress = false;
                     showstep_button_callback();
                     lock_clear();
                     showstep_next_callback();`);
        expect(dsvm.ds_state).to.include.key("oristick");
        expect(dsvm.ds_state.oristick).to.include.key("off");
    })
    it("when a button fails to setup, its label is 'ERROR' and other buttons still setup", () => {
        dsvm.vm.run(`the_show = new Show("$b.show");
                     section_play(the_show.playlist.get(0), true);
                     Ds.SetObjectArrayElemUsingRef(dsrefs.showstep.button, 1,
                                                   "Orion::system-toggle:oristick");
                     Ds.SetObjectArrayElemUsingRef(dsrefs.showstep.button, 2,
                                                   "CMa::bad-module:cmastick");
                     Ds.SetObjectArrayElemUsingRef(dsrefs.showstep.button, 3,
                                                   "CMi::system-toggle:cmistick");
                     showstep_button_callback_suppress = false;
                     showstep_button_callback();`);
        expect(dsvm.ds_state).to.include.keys("showstep_button1","showstep_button2","showstep_button3");
        expect(dsvm.ds_state.showstep_button1.label).to.equal("Orion");
        expect(dsvm.ds_state.showstep_button2.label).to.equal("ERROR");
        expect(dsvm.ds_state.showstep_button3.label).to.equal("CMi");
    })

})

describe("requires", () => {
    beforeEach(dsvm.initialize)

    it("runs a require in the first section of a show", () => {
        dsvm.add_file_mock("$main.show", `{
              "playlist": [
                {"section": "walk-in",
                 "require": ["system-toggle:oristick:name=orion"]},
                {"section": "introduction"}]}`);
        var lst = dsvm.vm.run(`
            the_show = new Show("$main.show");
            the_show.effects_on_list_for_section(the_show.playlist.get(0),
                                                 null);
            `);
        expect(lst.length).to.equal(1);
        expect(lst[0]).to.be.an.instanceof(dsvm.vm.run("EffectOnEvent"));
        expect(lst[0].effect.name).to.equal("orion");
    })
    it("turns off a require from the first section of a show", () => {
        dsvm.add_file_mock("$main.show", `{
              "playlist": [
                {"section": "walk-in",
                 "require": ["system-toggle:oristick:name=orion"]},
                {"section": "introduction"}]}`);
        var lst = dsvm.vm.run(`
            the_show = new Show("$main.show");
            the_show.effects_off_list_for_section(the_show.playlist.get(1));
            `);
        expect(lst.length).to.equal(1);
        expect(lst[0]).to.be.an.instanceof(dsvm.vm.run("EffectOffEvent"));
        expect(lst[0].effect.name).to.equal("orion");
    })
    it("runs a require for an autoplay in the default show", () => {
        dsvm.add_file_mock("$vignette.show", `{
              "playlist": [
                {"section": "v1",
                 "require": ["system-toggle:oristick:name=orion"],
                 "autoplay": true },
                {"section": "v2"}]}`);
        var lst = dsvm.vm.run(`
            the_show = new Show();
            Ds.SetObjectAttrUsingRef(dsrefs.showstep.insert,
                                     "$vignette.show");
            showstep_insert_callback();
            the_show.effects_on_list_for_section(the_show.playlist.get(0).playlist.get(0),
                                                 null);
            `);
        expect(lst.length).to.equal(1);
        expect(lst[0]).to.be.an.instanceof(dsvm.vm.run("EffectOnEvent"));
        expect(lst[0].effect.name).to.equal("orion");
    })
    it("turns off a require when advancing from a nested show", () => {
        dsvm.add_file_mock("$vignette.show", `{
              "playlist": [
                {"section": "v1",
                 "require": ["system-toggle:oristick:name=orion"]},
                {"section": "v2",
                 "require": ["@orion"]}
              ]
            }`);
        var lst = dsvm.vm.run(`
            the_show = new Show();
            the_show.playlist.insert(0,new Show("$vignette.show"));
            the_show.playlist.index = 0;
            the_show.playlist.get(0).playlist.index = 0;
            the_show.effects_off_list_for_section(null);
            `);
        expect(lst.length).to.equal(1);
        expect(lst[0]).to.be.an.instanceof(dsvm.vm.run("EffectOffEvent"));
        expect(lst[0].effect.name).to.equal("orion");
    })
})


describe("playing", () => {
    beforeEach(dsvm.initialize)

    it("when nested show is autoplay, only first section autoplays", () => {
        dsvm.add_file_mock("$main.show", `{
              "playlist": [
                {"section": "walk-in"},
                {"section": "introduction"}]}`);
        dsvm.add_file_mock("$vignette1.show", `{
              "autoplay": true,
              "playlist": [{"section": "vignette1", "lock": 0},
                           {"section": "vignette1.section2", "lock": 0}]}`);
        var s = dsvm.vm.run(`
                     the_show = new Show("$main.show");
                     the_show.playlist.index = 0;
                     Ds.SetObjectAttrUsingRef(dsrefs.showstep.insert,
                                              "$vignette1.show");
                     showstep_insert_callback();
                     the_show;`);
        expect(s.playlist.index).to.equal(1);
        expect(s.playlist.get(1).playlist.index).to.equal(0);

    })
    it("autoplay only plays if it is the very next section", () => {
        dsvm.add_file_mock("$main.show", `{
              "playlist": [
                {"section": "walk-in"},
                {"section": "introduction"}]}`);
        dsvm.add_file_mock("$vignette1.show", `{
              "autoplay": true,
              "playlist": [{"section": "vignette1a", "lock": 0},
                           {"section": "vignette1b", "lock": 0}]}`);
        dsvm.add_file_mock("$vignette2.show", `{
              "autoplay": true,
              "playlist": [{"section": "vignette2", "lock": 0}]}`);
        var s = dsvm.vm.run(`
                     the_show = new Show("$main.show");
                     var v1 = new Show("$vignette1.show");
                     the_show.playlist.insert(1, v1);
                     the_show.playlist.index = 1;
                     v1.playlist.index = 0;
                     Ds.SetObjectAttrUsingRef(dsrefs.showstep.insert, "$vignette2.show");
                     showstep_insert_callback();
                     the_show;`);
        expect(s.playlist.index).to.equal(1);
    })
    it("cleanup previous sibling show effects when advancing", () => {
        dsvm.add_file_mock("$vignette1.show", `{
              "autoplay": true,
              "playlist": [{"section": "vignette1a", "lock": 0,
                            "require": ["system-toggle:oristick:name=orion"]}]}`);
        dsvm.add_file_mock("$vignette2.show", `{
              "autoplay": true,
              "playlist": [{"section": "vignette2a", "lock": 0}]}`);
        var lst = dsvm.vm.run(`
                     the_show = new Show();
                     var v1 = new Show("$vignette1.show");
                     var v2 = new Show("$vignette2.show");
                     the_show.playlist.insert(0, v1);
                     the_show.playlist.insert(1, v2);
                     the_show.playlist.index = 0;
                     v1.playlist.index = 0;
                     v2.effects_off_list_for_section(v2.playlist.get(0));`);
        expect(lst.length).to.equal(1);
    })
    it("cleanup previous sibling show sections when advancing", () => {
        dsvm.add_file_mock("$vignette1.show", `{
              "autoplay": true,
              "playlist": [{"section": "vignette1a", "lock": 0,
                            "require": ["system-toggle:oristick:name=orion"]}]}`);
        dsvm.add_file_mock("$vignette2.show", `{
              "autoplay": true,
              "playlist": [{"section": "vignette2a", "lock": 0}]}`);
        var lst = dsvm.vm.run(`
                     the_show = new Show();
                     var v1 = new Show("$vignette1.show");
                     var v2 = new Show("$vignette2.show");
                     the_show.playlist.insert(0, v1);
                     the_show.playlist.insert(1, v2);
                     the_show.playlist.index = 0;
                     v1.playlist.index = 0;
                     v2.cleanup_list_for_section(v2.playlist.get(0));`);
        expect(lst.length).to.equal(1);
    })

})
